using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorInteraction : MonoBehaviour
{
    public GameObject redDoor; // La puerta roja
    public GameObject blueDoor; // La puerta azul
    private bool redDoorTouched = false; // Indica si la puerta roja fue tocada
    private bool blueDoorTouched = false; // Indica si la puerta azul fue tocada

    void Start()
    {
        // Aseg�rate de que las puertas est�n inicialmente invisibles
        redDoor.SetActive(true);
        blueDoor.SetActive(true);
    }

    void Update()
    {
        // Detectar clic del mouse en la puerta roja
        if (Input.GetMouseButtonDown(0)) // Clic izquierdo
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0; // Aseguramos que la posici�n z sea 0

            // Verificar si el clic fue sobre la puerta roja
            if (redDoor.GetComponent<Collider2D>().OverlapPoint(mousePos))
            {
                redDoorTouched = true;
            }

            // Verificar si el clic fue sobre la puerta azul
            if (blueDoor.GetComponent<Collider2D>().OverlapPoint(mousePos))
            {
                blueDoorTouched = true;
            }

            // Si ambas puertas han sido tocadas, pasamos a la siguiente escena
            if (redDoorTouched && blueDoorTouched)
            {
                SceneManager.LoadScene("Nivel 9"); // Cargar la siguiente escena (Nivel 9)
            }
        }
    }
}
