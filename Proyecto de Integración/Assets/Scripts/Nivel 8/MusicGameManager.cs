﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MusicGameManager : MonoBehaviour
{
    [System.Serializable]
    public class NoteButton
    {
        public string noteName; // Nombre de la nota (DO, RE, MI, etc.)
        public AudioClip sound; // Sonido que se reproducirá
        public bool isCorrect; // Indica si el sonido es correcto o incorrecto

        public Button noteButton; // Botón de la nota
        public Button checkButton; // Botón de correcto (✅)
        public Button wrongButton; // Botón de incorrecto (❌)
        public TMP_InputField noteInput; // Input para escribir la nota correcta
    }

    public NoteButton[] buttons; // Lista de botones de notas
    public GameObject[] lives; // Sprites de vidas
    public GameObject gameOverPanel; // Panel de Game Over
    public Button restartButton, menuButton, exitButton; // Botones del panel Game Over

    private int livesRemaining = 4; // Vidas del jugador
    private int currentIndex = 0; // Índice del botón activo

    public TMP_Text wellDoneText; // Texto "Bien hecho"
    public TMP_Text redDoorText; // Texto "Toca la puerta roja"
    public TMP_Text blueDoorText; // Texto "Toca la puerta azul"

    void Start()
    {
        AssignButtonListeners();
        gameOverPanel.SetActive(false); // Ocultar panel Game Over

        // Bloquear todos los botones excepto el primero
        for (int i = 1; i < buttons.Length; i++)
        {
            buttons[i].noteButton.interactable = false;
            buttons[i].checkButton.interactable = false;
            buttons[i].wrongButton.interactable = false;
            buttons[i].noteInput.interactable = false;
        }

        // Inicializar todos los inputs vacíos al principio
        foreach (var button in buttons)
        {
            button.noteInput.text = ""; // Vaciar el texto al iniciar
        }
    }


    void AssignButtonListeners()
    {
        foreach (var button in buttons)
        {
            button.noteButton.onClick.AddListener(() => PlayNoteSound(button));
            button.checkButton.onClick.AddListener(() => ValidateCorrect(button));
            button.wrongButton.onClick.AddListener(() => ValidateIncorrect(button));
            button.noteInput.onEndEdit.AddListener((input) => CheckInput(button, input));

            // Inicializar input deshabilitado
            button.noteInput.interactable = false;
            button.noteInput.text = "";
        }

        restartButton.onClick.AddListener(RestartLevel);
        menuButton.onClick.AddListener(GoToMenu);
        exitButton.onClick.AddListener(ExitGame);
    }

    void PlayNoteSound(NoteButton button)
    {
        AudioSource.PlayClipAtPoint(button.sound, Vector3.zero);

        // Si el sonido es incorrecto, activar el botón de ❌
        if (!button.isCorrect)
        {
            button.wrongButton.interactable = true;
        }
    }

    void ValidateCorrect(NoteButton button)
    {
        if (button.isCorrect)
        {
            button.checkButton.interactable = false; // Desactiva el botón Correcto (✅) para evitar múltiples clics
            UnlockNextButton(button);
        }
        else
        {
            LoseLife();
        }
    }


    void ValidateIncorrect(NoteButton button)
    {
        if (!button.isCorrect)
        {
            button.noteInput.interactable = true; // Activa el input solo si es incorrecto
            button.noteInput.text = "";
            button.noteInput.image.color = Color.white;
        }
        else
        {
            LoseLife();
        }
    }


    void CheckInput(NoteButton button, string inputText)
    {
        if (!button.noteInput.interactable) return; // Evita múltiples validaciones

        string correctNote = GetCorrectNote(button.noteName);

        if (inputText.Trim().ToUpper() == correctNote.ToUpper())
        {
            button.noteInput.image.color = Color.green;
            button.noteInput.DeactivateInputField(); // Evita que el jugador siga escribiendo, pero no lo bloquea completamente

            UnlockNextButton(button); // Desbloquea SOLO el siguiente botón

            // Si es el último botón, activa los textos finales
            if (button == buttons[buttons.Length - 1])
            {
                wellDoneText.gameObject.SetActive(true);
                redDoorText.gameObject.SetActive(true);
                blueDoorText.gameObject.SetActive(true);
            }
        }
        else
        {
            button.noteInput.image.color = Color.red;
            LoseLife();
        }
    }

    void UnlockNextButton(NoteButton currentButton)
    {
        int index = System.Array.IndexOf(buttons, currentButton);

        if (index >= 0 && index < buttons.Length - 1)
        {
            NoteButton nextButton = buttons[index + 1];
            nextButton.noteButton.interactable = true;
            nextButton.checkButton.interactable = true;
            nextButton.wrongButton.interactable = true;
        }
    }



    // Método para deshabilitar el input después de un breve delay
    void DisableInputField()
    {
        foreach (var button in buttons)
        {
            button.noteInput.interactable = false;
        }
    }

    string GetCorrectNote(string incorrectNote)
    {
        switch (incorrectNote)
        {
            case "DO": return "SOL";  // Si DO suena como SOL, la respuesta correcta es SOL
            case "RE": return "RE";   // Ajusta según los sonidos incorrectos asignados
            case "MI": return "LA";
            case "FA": return "FA";
            case "SOL": return "SI";
            case "LA": return "LA";
            case "SI": return "DO";
            default: return ""; // No debería ocurrir, pero previene errores
        }
    }

    void UnlockNextButton()
    {
        if (currentIndex < buttons.Length - 1)
        {
            buttons[currentIndex].noteInput.interactable = false;
            currentIndex++;

            buttons[currentIndex].noteButton.interactable = true;
            buttons[currentIndex].checkButton.interactable = true;
            buttons[currentIndex].wrongButton.interactable = true;
        }
    }

    void LoseLife()
    {
        if (livesRemaining > 0)
        {
            lives[livesRemaining - 1].SetActive(false); // Oculta una vida
            livesRemaining--;

            if (livesRemaining == 0)
            {
                GameOver();
            }
        }
    }

    void GameOver()
    {
        gameOverPanel.SetActive(true);
    }

    void RestartLevel()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    void GoToMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }

    void ExitGame()
    {
        Application.Quit();
    }
}


