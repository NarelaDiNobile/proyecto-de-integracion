using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Instrucciones : MonoBehaviour
{
    public TMP_Text timeText;   // Referencia al texto del contador
    public GameObject panelStart;   // El panel negro con el contador y texto
    private float currentTime = 5400f;   // 1 hora y 30 minutos en segundos
    private bool gameStarted = false;

    public static Instrucciones instance;
    [SerializeField] Animator transitionAnim;
    void Start()
    {
        timeText.text = "01:30:00";  // Configura el contador como "1:30:00" desde el principio
        panelStart.SetActive(false); // El panel est� desactivado al principio
    }

    void Update()
    {
        if (gameStarted)
        {
            // Reducir el tiempo en cada frame
            currentTime -= Time.deltaTime;
            UpdateTimeDisplay();

            if (currentTime <= 0)
            {
                currentTime = 0;
                GameOver();
            }
        }
    }

    // Actualiza el texto del contador en formato "HH:MM:SS"
    void UpdateTimeDisplay()
    {
        int hours = Mathf.FloorToInt(currentTime / 3600);  // Calcular horas
        int minutes = Mathf.FloorToInt((currentTime % 3600) / 60);  // Calcular minutos
        int seconds = Mathf.FloorToInt(currentTime % 60);  // Calcular segundos
        timeText.text = string.Format("{0:00}:{1:00}:{2:00}", hours, minutes, seconds);  // Mostrar como "HH:MM:SS"
    }

    // Iniciar el juego cuando el jugador presiona "Comenzar"
    public void StartGame()
    {
        panelStart.SetActive(true);  // Activar el panel negro con el contador
        gameStarted = true;           // Iniciar el conteo
        StartCoroutine(ShowCounterAndStartLevel()); // Mostrar el contador y luego pasar al nivel
    }

    // Corutina para mostrar el contador durante 3 segundos y luego pasar al nivel
    IEnumerator ShowCounterAndStartLevel()
    {
        yield return new WaitForSeconds(3f);  // Esperar 3 segundos con el contador visible
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        transitionAnim.SetTrigger("End");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        transitionAnim.SetTrigger("Start");
    }

    // Game Over
    void GameOver()
    {
        // Mostrar panel de Game Over y detener el juego
        Debug.Log("Game Over");
        // Aqu� puedes a�adir la l�gica de "Game Over"
    }

    // Funci�n que muestra el contador cuando el jugador presiona TAB
    public void ShowTime()
    {
        // Mostrar el tiempo restante en pantalla
        timeText.enabled = true;
        Invoke("HideTime", 2f);  // Desactivar el texto despu�s de 2 segundos
    }

    // Ocultar el texto del tiempo
    void HideTime()
    {
        timeText.enabled = false;
    }
}
