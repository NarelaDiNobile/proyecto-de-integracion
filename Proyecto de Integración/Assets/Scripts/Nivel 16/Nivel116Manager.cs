using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement; // Importar para cambiar de escena
using System.Collections;

public class Nivel116Manager : MonoBehaviour
{
    [Header("Textos")]
    public TMP_Text txtPreguntaInicial;
    public TMP_Text txtPreguntaBotones;
    public TMP_Text txtResultadoCorrecto;
    public TMP_Text txtResultadoIncorrecto;
    public TMP_Text txtDudaIntuicion;
    public TMP_Text txtTocarPuertaRoja;
    public TMP_Text txtTocarPuertaAzul;

    [Header("Botones")]
    public Button btnSi;
    public Button btnNo;
    public Button btnCorrecto;
    public Button btnIncorrecto;

    [Header("Puertas")]
    public GameObject puertaRoja;
    public GameObject puertaAzul;

    private bool puertaRojaTocada = false;
    private bool puertaAzulTocada = false;
    private bool esOpcionCorrecta = false; // Para saber si debe ir a nivel 18 o 19

    public static NivelController instance;
    [SerializeField] Animator transitionAnim;

    void Start()
    {
        // Desactivar elementos al inicio
        txtPreguntaBotones.gameObject.SetActive(false);
        txtResultadoCorrecto.gameObject.SetActive(false);
        txtResultadoIncorrecto.gameObject.SetActive(false);
        txtDudaIntuicion.gameObject.SetActive(false);
        txtTocarPuertaRoja.gameObject.SetActive(false);
        txtTocarPuertaAzul.gameObject.SetActive(false);
        btnCorrecto.gameObject.SetActive(false);
        btnIncorrecto.gameObject.SetActive(false);
        puertaRoja.SetActive(false);
        puertaAzul.SetActive(false);

        // Asignar eventos a los botones
        btnSi.onClick.AddListener(OpcionSi);
        btnNo.onClick.AddListener(OpcionNo);
        btnCorrecto.onClick.AddListener(SeleccionCorrecta);
        btnIncorrecto.onClick.AddListener(SeleccionIncorrecta);
    }

    void OpcionSi()
    {
        txtPreguntaInicial.gameObject.SetActive(false);
        btnSi.gameObject.SetActive(false);
        btnNo.gameObject.SetActive(false);
        txtPreguntaBotones.gameObject.SetActive(true);
        btnCorrecto.gameObject.SetActive(true);
        btnIncorrecto.gameObject.SetActive(true);
    }

    void OpcionNo()
    {
        txtPreguntaInicial.gameObject.SetActive(false);
        btnSi.gameObject.SetActive(false);
        btnNo.gameObject.SetActive(false);
        txtDudaIntuicion.gameObject.SetActive(true);
        btnCorrecto.gameObject.SetActive(true);
        btnIncorrecto.gameObject.SetActive(true);
    }

    void SeleccionCorrecta()
    {
        txtDudaIntuicion.gameObject.SetActive(false);
        btnCorrecto.gameObject.SetActive(false);
        btnIncorrecto.gameObject.SetActive(false);
        txtPreguntaBotones.gameObject.SetActive(false);

        // Activar puertas y textos
        puertaRoja.SetActive(true);
        puertaAzul.SetActive(true);
        txtTocarPuertaRoja.gameObject.SetActive(true);
        txtTocarPuertaAzul.gameObject.SetActive(true);

        // Indicar que es la opci�n correcta
        esOpcionCorrecta = true;

        // Mostrar mensaje de �xito
        txtResultadoCorrecto.gameObject.SetActive(true);
    }

    void SeleccionIncorrecta()
    {
        txtDudaIntuicion.gameObject.SetActive(false);
        btnCorrecto.gameObject.SetActive(false);
        btnIncorrecto.gameObject.SetActive(false);
        txtPreguntaBotones.gameObject.SetActive(false);

        // Activar puertas y textos
        puertaRoja.SetActive(true);
        puertaAzul.SetActive(true);
        txtTocarPuertaRoja.gameObject.SetActive(true);
        txtTocarPuertaAzul.gameObject.SetActive(true);

        // Indicar que es la opci�n incorrecta
        esOpcionCorrecta = false;

        // Mostrar mensaje de error
        txtResultadoIncorrecto.gameObject.SetActive(true);
    }

    void VerificarPuertas()
    {
        StartCoroutine(LoadLevel());
    }


    IEnumerator LoadLevel()
    {
        if (puertaRojaTocada && puertaAzulTocada)
        {
            if (esOpcionCorrecta)
            {
                transitionAnim.SetTrigger("End");
                yield return new WaitForSeconds(1);
                SceneManager.LoadScene(19); // Ir al nivel 19
                transitionAnim.SetTrigger("Start");
            }
            else
            {
                transitionAnim.SetTrigger("End");
                yield return new WaitForSeconds(1);
                SceneManager.LoadScene(18); 
                transitionAnim.SetTrigger("Start");
            }
        }

    }




    void Update()
    {
        if (Input.GetMouseButtonDown(0)) // Detectar clic
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == puertaRoja)
                {
                    puertaRojaTocada = true;
                    txtTocarPuertaRoja.gameObject.SetActive(false);
                }
                else if (hit.collider.gameObject == puertaAzul)
                {
                    puertaAzulTocada = true;
                    txtTocarPuertaAzul.gameObject.SetActive(false);
                }

                VerificarPuertas();
            }
        }
    }
}
