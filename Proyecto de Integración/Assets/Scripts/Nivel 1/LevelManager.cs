using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelManager : MonoBehaviour
{
    public TMP_Text[] feedbackTexts; // Array de textos de feedback (Incorrecto/Correcto)
    public TMP_Text doorText; // Texto para la puerta roja
    public TMP_Text blueDoorText; // Texto para la puerta azul
    public GameObject[] lifeSprites; // Los sprites de vida
    public GameObject door1, door2; // Puertas del nivel (sprites)
    public Button[] colorButtons; // Botones de colores
    public GameObject gameOverPanel; // Panel de juego terminado
    public Button restartButton, menuButton, exitButton; // Botones del panel de juego terminado

    private int lives = 4; // N�mero de vidas inicial
    private bool isAnswerCorrect = false; // Bandera para evitar m�ltiples respuestas
    private bool door1Clicked = false; // Para verificar si la puerta roja fue clicada
    private bool door2Clicked = false; // Para verificar si la puerta azul fue clicada

    public static LevelManager instance;
    [SerializeField] Animator transitionAnim;

    void Start()
    {
        // Inicializar feedbacks invisibles
        foreach (TMP_Text text in feedbackTexts)
        {
            if (text != null)
            {
                text.gameObject.SetActive(false); // Solo activar si no es nulo
            }
        }

        // Inicializar puertas bloqueadas
        if (doorText != null) doorText.text = "Puerta bloqueada";
        if (blueDoorText != null) blueDoorText.text = "Puerta bloqueada";

        // Configurar botones de colores
        for (int i = 0; i < colorButtons.Length; i++)
        {
            int buttonIndex = i; // Capturar �ndice de cada bot�n
            colorButtons[i].onClick.AddListener(() => CheckAnswer(buttonIndex));
        }

        // Configurar botones del panel de juego terminado
        restartButton.onClick.AddListener(RestartLevel);
        menuButton.onClick.AddListener(GoToMainMenu);
        exitButton.onClick.AddListener(ExitGame);

        // Desactivar el panel de juego terminado al inicio
        if (gameOverPanel != null)
        {
            gameOverPanel.SetActive(false);
        }
    }

    void CheckAnswer(int buttonIndex)
    {
        if (isAnswerCorrect) return; // Si ya respondi� correctamente, no hacer nada m�s

        // Resetear feedbacks visibles
        foreach (TMP_Text text in feedbackTexts)
        {
            if (text != null)
            {
                text.gameObject.SetActive(false); // Solo desactivar si no es nulo
            }
        }

        if (buttonIndex == 3) // Verde es la respuesta correcta
        {
            if (feedbackTexts[buttonIndex] != null)
            {
                feedbackTexts[buttonIndex].gameObject.SetActive(true); // Mostrar "Correcto"
                feedbackTexts[buttonIndex].text = "Correcto";
            }
            UnlockDoors();
            DisableIncorrectButtons(); // Deshabilitar botones incorrectos
            isAnswerCorrect = true; // Marcar la respuesta como correcta
        }
        else
        {
            if (feedbackTexts[buttonIndex] != null)
            {
                feedbackTexts[buttonIndex].gameObject.SetActive(true); // Mostrar "Incorrecto"
                feedbackTexts[buttonIndex].text = "Incorrecto";
            }
            LoseLife();
        }
    }

    void LoseLife()
    {
        lives--;
        if (lives <= 0)
        {
            GameOver();
        }
        else
        {
            // Desactivar el sprite correspondiente a la vida perdida
            lifeSprites[lives].SetActive(false);
        }
    }

    void GameOver()
    {
        gameOverPanel.SetActive(true); // Activar el panel de juego terminado
        foreach (Button btn in colorButtons)
        {
            btn.interactable = false; // Deshabilitar botones de respuesta
        }
    }

    void UnlockDoors()
    {
        doorText.text = "Puerta desbloqueada";
        blueDoorText.text = "Puerta desbloqueada";
    }

    void DisableIncorrectButtons()
    {
        for (int i = 0; i < colorButtons.Length; i++)
        {
            if (i != 3) // Deshabilita todos excepto el bot�n verde (�ndice 3)
            {
                colorButtons[i].interactable = false;
            }
        }
    }

    void Update()
    {
        if (isAnswerCorrect)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                if (door1.GetComponent<Collider2D>().OverlapPoint(mousePos))
                {
                    door1Clicked = true;
                }
                if (door2.GetComponent<Collider2D>().OverlapPoint(mousePos))
                {
                    door2Clicked = true;
                }
                 LoadNextLevel();
            }
        }
    }

    public void LoadNextLevel()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if (door1Clicked && door2Clicked)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // Reiniciar nivel actual
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene("Menu"); // Cargar el men� principal (debe existir una escena llamada MainMenu)
    }

    public void ExitGame()
    {
        Application.Quit(); // Cerrar el juego
        Debug.Log("Juego cerrado"); // Mensaje para pruebas en Unity Editor
    }
}
