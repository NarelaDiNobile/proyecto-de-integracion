using UnityEngine;
using UnityEngine.UI;

public class Instrucciones1 : MonoBehaviour
{
    public GameObject instruccionesPanel;  // El panel con las instrucciones
    public Button cerrarButton;            // El bot�n para cerrar las instrucciones
    public Animator anim;                   // Animator para la animaci�n

    void Start()
    {
        // Asegurarse de que las instrucciones se muestren al principio
        instruccionesPanel.SetActive(true);

        // Asignar la funci�n de cierre al bot�n
        cerrarButton.onClick.AddListener(CerrarInstrucciones);
    }

    // Funci�n para cerrar las instrucciones con animaci�n
    public void CerrarInstrucciones()
    {
        anim.SetTrigger("Cerrar"); // Activa la animaci�n de cierre
    }

    void DesactivarPanel()
    {
        instruccionesPanel.SetActive(false); // Desactiva el panel despu�s de la animaci�n
    }
}
