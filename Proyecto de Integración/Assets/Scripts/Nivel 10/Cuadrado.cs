using System.Collections;
using TMPro;
using UnityEngine;

public class Cuadrado : MonoBehaviour
{
    public DibujarCamino drawControll;
    public ControladorCuadrados controladorCuadrados;
    public float speed = 10f;
    private Vector3[] positions;
    private bool startMovement = false;
    private int moveIndex = 0;
    private bool caminoCompleto = false;
    private bool isPaused = false;

    public Vector3 posicionInicial;

    private bool yaDescontado = false;

    public TMP_Text textoAzul, textoRojo; // Declaramos los textos

    public GameObject puertaRoja, puertaAzul; // Las puertas como GameObject

    public bool haLlegado = false; // Variable para saber si ha llegado a su destino

    private Vector3 destino;  // El destino al que debe llegar el cuadrado

    private void Start()
    {
        posicionInicial = transform.position;
    }

    private void OnMouseDown()
    {
        drawControll.StartLine(transform.position);
    }

    private void OnMouseDrag()
    {
        drawControll.UpdateLine();
    }

    private void OnMouseUp()
    {
        positions = new Vector3[drawControll.line.positionCount];
        drawControll.line.GetPositions(positions);

        if (positions != null && positions.Length > 0)
        {
            caminoCompleto = true;
            controladorCuadrados.VerificarCaminoCompleto(this);
        }
    }

    public bool TieneCaminoCompleto()
    {
        return caminoCompleto;
    }

    public void IniciarMovimiento()
    {
        startMovement = true;
        moveIndex = 0;
    }

    private void Update()
    {
        if (isPaused || positions == null || positions.Length == 0) return;

        if (startMovement)
        {
            Vector2 currentPos = positions[moveIndex];
            transform.position = Vector2.MoveTowards(transform.position, currentPos, speed * Time.deltaTime);

            if (Vector2.Distance(currentPos, transform.position) <= 0.05f)
            {
                moveIndex++;
            }

            if (moveIndex >= positions.Length)
            {
                startMovement = false;

                // Cuando llega al destino, activamos los textos
                textoAzul.gameObject.SetActive(true);
                textoRojo.gameObject.SetActive(true);
                haLlegado = true;  // Marca como que ha llegado al destino
            }
        }
    }

    // Aseg�rate de que las puertas tienen el componente Collider2D y la opci�n "Is Trigger" activada.

    private void OnTriggerEnter2D(Collider2D other)
    {
        // Verificamos si el objeto que colisiona es otro cuadrado
        if (other.CompareTag("Cuadrado") && other != this.GetComponent<Collider2D>())
        {
            // Aqu� restamos una vida y verificamos si el cuadrado debe ser reiniciado
            controladorCuadrados.PerderVida();

            // Puedes agregar una l�gica extra como pausar el cuadrado por un tiempo o reiniciarlo
            ResetearPosicion();  // Ejemplo de reiniciar el cuadrado al chocar
        }
    }


    private bool TodosLosCuadradosHanLlegado()
    {
        // Accedemos a la lista de cuadrados desde el controlador
        foreach (var cuadrado in controladorCuadrados.cuadrados)
        {
            if (!cuadrado.HaLlegadoADestino())  // Verificamos si el cuadrado ha llegado a su destino
            {
                return false;
            }
        }
        return true;  // Si todos los cuadrados llegaron a su destino
    }



    private void CambiarDeNivel()
    {
        // Aqu� puedes agregar la l�gica para cambiar al siguiente nivel, por ejemplo:
        UnityEngine.SceneManagement.SceneManager.LoadScene("Nivel 11");
    }


    private IEnumerator ResetearFlag()
    {
        // Esperamos un tiempo para resetear el flag y permitir futuras colisiones
        yield return new WaitForSeconds(0.5f);
        yaDescontado = false;  // Resetear el flag para que el cubo pueda descontar otra vida m�s adelante
    }

    public void LimpiarCamino()
    {
        caminoCompleto = false;
        drawControll.line.positionCount = 0;
        positions = null;
    }

    public void ResetearPosicion()
    {
        transform.position = posicionInicial;
        caminoCompleto = false;
        positions = null;
        drawControll.line.positionCount = 0;
        startMovement = false;
        haLlegado = false;  // Reseteamos el estado de llegada al destino
    }

    public void PausarMovimiento()
    {
        isPaused = true;
    }

    public void RestablecerMovimiento()
    {
        isPaused = false;
    }

    // Nuevo m�todo para verificar si el cuadrado ha llegado a su destino
    public bool HaLlegadoADestino()
    {
        return haLlegado;
    }

}
