using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;

public class ControladorPuertas : MonoBehaviour
{
    public GameObject puertaAzul;
    public GameObject puertaRoja;
    public TMP_Text textoAzul;
    public TMP_Text textoRojo;

    private bool puertaAzulClickeada = false;
    private bool puertaRojaClickeada = false;

    public static ControladorPuertas instance;
    [SerializeField] Animator transitionAnim;

    private void Update()
    {
        // Detectamos el clic en la puerta azul
        if (Input.GetMouseButtonDown(0)) // 0 es el bot�n izquierdo del rat�n
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == puertaAzul)
                {
                    puertaAzulClickeada = true;
                    textoAzul.text = "�Puerta azul clickeada!";
                }
                else if (hit.collider.gameObject == puertaRoja)
                {
                    puertaRojaClickeada = true;
                    textoRojo.text = "�Puerta roja clickeada!";
                }
            }
        }

        // Comprobar si ambas puertas han sido clickeadas
        if (puertaAzulClickeada && puertaRojaClickeada)
        {
            CambiarDeNivel();
        }
    }

    private void CambiarDeNivel()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if (puertaAzulClickeada && puertaRojaClickeada)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}
