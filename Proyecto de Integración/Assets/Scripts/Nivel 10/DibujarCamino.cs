using UnityEngine;

public class DibujarCamino : MonoBehaviour
{
    public LineRenderer line;
    private Vector3 previospPosition;

    [SerializeField]
    private float minDistance = 0.1f;

    [SerializeField, Range(0.1f, 2f)]
    private float width;

    private void Start()
    {
        line = GetComponent<LineRenderer>();
        line.positionCount = 1;
        previospPosition = transform.position;
        line.startWidth = line.endWidth = width;
    }

    public void StartLine(Vector2 position)
    {
        line.positionCount = 1;
        line.SetPosition(0, position);
    }

    public void UpdateLine()
    {
        if (Input.GetMouseButton(0))
        {
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            currentPosition.z = 0;

            if (Vector3.Distance(currentPosition, previospPosition) > minDistance)
            {
                if (previospPosition == transform.position)
                {
                    line.SetPosition(0, currentPosition);
                }
                else
                {
                    line.positionCount++;
                    line.SetPosition(line.positionCount - 1, currentPosition);
                }

                previospPosition = currentPosition;
            }
        }
    }

    // M�todo para resetear el LineRenderer (limpiar el camino dibujado)
    public void ResetLine()
    {
        line.positionCount = 0; // Limpiar los puntos del LineRenderer
    }
}




