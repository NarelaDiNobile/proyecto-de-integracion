using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ControladorCuadrados : MonoBehaviour
{
    public List<Cuadrado> cuadrados; // Lista de objetos Cuadrado
    public Button botonIniciar;
    public GameObject[] vidas; // Sprites de las vidas
    public GameObject gameOverPanel; // Panel de Game Over
    public TMP_Text textoAzul, textoRojo; // Textos "Tocar puerta azul" y "Tocar puerta roja"
    public GameObject puertaAzul, puertaRoja; // Puertas para pasar de nivel

    private int vidasRestantes;

    private void Start()
    {
        if (cuadrados == null || cuadrados.Count == 0)
        {
            cuadrados = new List<Cuadrado>(FindObjectsOfType<Cuadrado>()); // Busca todos los objetos Cuadrado si la lista est� vac�a
        }

        if (cuadrados.Count == 0)
        {
            Debug.LogError("La lista de cuadrados est� vac�a o no se han encontrado objetos Cuadrado en la escena.");
            return;
        }

        vidasRestantes = vidas.Length;
        ActualizarVidas();

        botonIniciar.interactable = false;  // Aseguramos que el bot�n Iniciar est� desactivado al inicio
        botonIniciar.onClick.AddListener(IniciarMovimiento);
        gameOverPanel.SetActive(false);
        textoAzul.gameObject.SetActive(false);
        textoRojo.gameObject.SetActive(false);
        puertaAzul.SetActive(true);
        puertaRoja.SetActive(true);
    }

    public bool TodosLosCuadradosHanLlegado()
    {
        foreach (var cuadrado in cuadrados)
        {
            if (!cuadrado.HaLlegadoADestino())  // Si alg�n cuadrado no ha llegado, retorna false
            {
                return false;
            }
        }
        return true;  // Si todos los cuadrados han llegado a su destino, retorna true
    }


    public void IniciarMovimiento()
    {
        foreach (var cuadrado in cuadrados)
        {
            cuadrado.IniciarMovimiento();
        }
    }

    public void ActivarBotonIniciar()
    {
        foreach (var cuadrado in cuadrados)
        {
            if (!cuadrado.TieneCaminoCompleto())
            {
                botonIniciar.interactable = false;
                return;  // Si no todos los caminos est�n completos, desactiva el bot�n y sale
            }
        }
        botonIniciar.interactable = true;  // Si todos los caminos est�n completos, activa el bot�n
    }

    public void VerificarCaminoCompleto(Cuadrado cuadrado)
    {
        ActivarBotonIniciar();  // Verifica si todos los caminos est�n completos
        ComprobarDestinos();  // Verifica si todos los cuadrados llegaron a su destino
    }

    private void ComprobarDestinos()
    {
        // Verifica si todos los cuadrados han llegado a su destino
        bool todosEnDestino = true;
        foreach (var cuadrado in cuadrados)
        {
            if (!cuadrado.HaLlegadoADestino())  // Comprobamos si ha llegado a su destino
            {
                todosEnDestino = false;
                break;  // Si alguno no ha llegado, salimos
            }
        }

        if (todosEnDestino)
        {
            // Si todos llegaron a su destino, activamos las im�genes de las puertas
            puertaAzul.gameObject.SetActive(true); // Muestra la puerta azul
            puertaRoja.gameObject.SetActive(true); // Muestra la puerta roja
            textoAzul.gameObject.SetActive(true);  // Activa el texto de la puerta azul
            textoRojo.gameObject.SetActive(true);  // Activa el texto de la puerta roja
        }
    }



    public void PerderVida()
    {
        if (vidasRestantes > 0)
        {
            vidasRestantes--;
            ActualizarVidas();

            if (vidasRestantes <= 0)
            {
                gameOverPanel.SetActive(true);  // Mostrar el panel de Game Over cuando se acaben las vidas
            }
            else
            {
                StartCoroutine(PausarYReiniciarNivel());
            }
        }
    }


    private IEnumerator PausarYReiniciarNivel()
    {
        foreach (var cuadrado in cuadrados)
        {
            cuadrado.PausarMovimiento();
        }

        yield return new WaitForSeconds(2f);

        foreach (var cuadrado in cuadrados)
        {
            cuadrado.ResetearPosicion();
            cuadrado.RestablecerMovimiento();
        }
    }

    private void ActualizarVidas()
    {
        for (int i = 0; i < vidas.Length; i++)
        {
            vidas[i].SetActive(i < vidasRestantes);
        }
    }


    public void ReiniciarNivel()
    {
        gameOverPanel.SetActive(false);

        vidasRestantes = vidas.Length;
        ActualizarVidas();

        foreach (var cuadrado in cuadrados)
        {
            cuadrado.ResetearPosicion();
            cuadrado.RestablecerMovimiento();
            cuadrado.LimpiarCamino();
        }

        botonIniciar.interactable = false;  // Aseguramos que el bot�n Iniciar est� desactivado despu�s de reiniciar
        puertaAzul.gameObject.SetActive(true); // Muestra la puerta azul
        puertaRoja.gameObject.SetActive(true); // Muestra la puerta roja
        textoAzul.gameObject.SetActive(false);  // Activa el texto de la puerta azul
        textoRojo.gameObject.SetActive(false);  // Activa el texto de la puerta roja
    }


    public void VolverAlMenu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }

    public void SalirDelJuego()
    {
        Application.Quit();
    }
}
