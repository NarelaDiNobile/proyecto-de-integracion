using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class DoorController : MonoBehaviour
{
    public TMP_Text doorText;  // Referencia al TMP_Text de la puerta
    private static bool redDoorClicked = false;  // Estado del clic en la puerta roja
    private static bool blueDoorClicked = false; // Estado del clic en la puerta azul
    private static bool redDoorUnlocked = false;  // Estado de desbloqueo de la puerta roja
    private static bool blueDoorUnlocked = false; // Estado de desbloqueo de la puerta azul

    private void Start()
    {
        // Comprobamos si las puertas ya est�n desbloqueadas al iniciar
        if (doorText.text == "Puerta desbloqueada")
        {
            if (gameObject.name == "Puerta Roja")
            {
                redDoorUnlocked = true;
            }
            else if (gameObject.name == "Puerta Azul")
            {
                blueDoorUnlocked = true;
            }
        }
    }

    private void OnMouseDown()
    {
        if ((gameObject.name == "Puerta Roja" && redDoorUnlocked) ||
            (gameObject.name == "Puerta Azul" && blueDoorUnlocked))
        {
            if (gameObject.name == "Puerta Roja")
            {
                redDoorClicked = true;
            }
            else if (gameObject.name == "Puerta Azul")
            {
                blueDoorClicked = true;
            }

            CheckDoorsClicked();  // Verificar si ambas puertas fueron clicadas
        }
        else
        {
            Debug.Log("La puerta a�n est� bloqueada.");
        }
    }


    // M�todo para desbloquear la puerta (lo llamaremos cuando los fuegos se apaguen)
    public void UnlockDoor()
    {
        doorText.text = "Puerta desbloqueada";

        if (gameObject.name == "Puerta Roja")
        {
            redDoorUnlocked = true;
        }
        else if (gameObject.name == "Puerta Azul")
        {
            blueDoorUnlocked = true;
        }
    }

    // M�todo para verificar si ambas puertas fueron clicadas
    private void CheckDoorsClicked()
    {
        if (redDoorClicked && blueDoorClicked)
        {
            Debug.Log("Ambas puertas han sido clicadas. Cambiando a Nivel 3...");
            SceneManager.LoadScene("Nivel 3");
        }
    }
}
