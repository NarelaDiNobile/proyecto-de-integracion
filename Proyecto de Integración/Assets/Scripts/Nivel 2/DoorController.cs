using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;

public class DoorController : MonoBehaviour
{
    public TMP_Text doorText;  // Referencia al TMP_Text de la puerta
    private static bool redDoorClicked = false;  // Estado del clic en la puerta roja
    private static bool blueDoorClicked = false; // Estado del clic en la puerta azul
    private static bool redDoorUnlocked = true;  // Estado de desbloqueo de la puerta roja
    private static bool blueDoorUnlocked = true; // Estado de desbloqueo de la puerta azul

    public static DoorController instance;
    [SerializeField] Animator transitionAnim;

    private void Start()
    {
        // Comprobamos si las puertas ya est�n desbloqueadas al iniciar
        if (doorText.text == "Puerta desbloqueada")
        {
            if (gameObject.name == "Puerta Roja")
            {
                redDoorUnlocked = true;
            }
            else if (gameObject.name == "Puerta Azul")
            {
                blueDoorUnlocked = true;
            }
        }
    }

    private void OnMouseDown()
    {
        if ((gameObject.name == "Puerta Roja" && redDoorUnlocked) ||
            (gameObject.name == "Puerta Azul" && blueDoorUnlocked))
        {
            if (gameObject.name == "Puerta Roja")
            {
                redDoorClicked = true;
            }
            else if (gameObject.name == "Puerta Azul")
            {
                blueDoorClicked = true;
            }

            CheckDoorsClicked();  // Verificar si ambas puertas fueron clicadas
        }
        else
        {
            Debug.Log("La puerta a�n est� bloqueada.");
        }
    }


    // M�todo para desbloquear la puerta (lo llamaremos cuando los fuegos se apaguen)
    public void UnlockDoor()
    {
        doorText.text = "Puerta desbloqueada";

        if (gameObject.name == "Puerta Roja")
        {
            redDoorUnlocked = true;
        }
        else if (gameObject.name == "Puerta Azul")
        {
            blueDoorUnlocked = true;
        }
    }

    // M�todo para verificar si ambas puertas fueron clicadas
    public void CheckDoorsClicked()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if (redDoorClicked && blueDoorClicked)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}
