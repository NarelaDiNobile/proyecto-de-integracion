using UnityEngine;
using TMPro;  // Necesario para usar TextMeshPro

public class WaterCollision : MonoBehaviour
{
    private int firesExtinguished = 0;  // Contador de fuegos apagados
    public int totalFires = 23;  // Total de fuegos en el nivel

    public TMP_Text redDoorText;  // Referencia al texto de la puerta roja
    public TMP_Text blueDoorText; // Referencia al texto de la puerta azul

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Fuego"))
        {
            other.gameObject.SetActive(false);  // Desactivar el fuego al contacto con el agua
            firesExtinguished++;  // Incrementar el contador
            Debug.Log("Fuego apagado: " + firesExtinguished);

            // Verificar si todos los fuegos se apagaron
            if (firesExtinguished >= totalFires)
            {
                UnlockDoors();
            }
        }
    }

    // M�todo para desbloquear las puertas mostrando el texto
    private void UnlockDoors()
    {
        if (redDoorText != null && blueDoorText != null)
        {
            redDoorText.text = "Puerta desbloqueada";
            blueDoorText.text = "Puerta desbloqueada";
            Debug.Log("�Puertas desbloqueadas!");
        }
    }
}
