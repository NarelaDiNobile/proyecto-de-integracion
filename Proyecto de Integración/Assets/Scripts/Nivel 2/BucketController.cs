using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BucketController : MonoBehaviour
{
    public Sprite emptyBucketSprite;  // Sprite del balde vac�o en el Mundo Malo
    public Sprite filledBucketSprite; // Sprite del balde lleno en el Mundo Bueno
    public Slider waterBar;
    public TMP_Text waterPercentageText;
    public float fillSpeed = 20f;
    public Vector3 startPosition;
    public GameObject[] lifeSprites;  // Array de sprites de vidas
    public float fallLimit = -13f;  // L�mite de ca�da en Y donde se pierde una vida
    public Vector3 teleportPosition;  // Posici�n a donde se teletransportar� el balde en el Mundo Bueno
    public GameObject filledBucketInWorldGood; // El balde lleno en el Mundo Bueno

    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;
    private bool isFilling = false;
    private bool isFilled = false;
    private bool isDragging = false;
    private Vector3 offset;
    private Camera mainCamera;
    private int livesRemaining;

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        startPosition = transform.position;  // Guardar la posici�n inicial
        waterBar.value = 0;
        spriteRenderer.sprite = emptyBucketSprite;  // El balde inicia vac�o
        mainCamera = Camera.main;
        rb.gravityScale = 0;  // Inicialmente sin gravedad
        UpdateWaterPercentageText();
        livesRemaining = lifeSprites.Length;

        // Aseg�rate de que el balde lleno en el Mundo Bueno est� desactivado al inicio
        filledBucketInWorldGood.SetActive(false);
    }

    void Update()
    {
        // Rellenar el balde si est� bajo el agua
        if (isFilling && !isFilled)
        {
            waterBar.value += fillSpeed * Time.deltaTime;
            UpdateWaterPercentageText();

            if (waterBar.value >= waterBar.maxValue)
            {
                waterBar.value = waterBar.maxValue;
                isFilled = true;
                spriteRenderer.sprite = filledBucketSprite;  // Cambiar a balde lleno
                Debug.Log("El balde est� lleno.");
            }
        }

        // Si el balde ha ca�do por debajo del l�mite, perder una vida y reiniciar
        if (transform.position.y <= fallLimit)
        {
            LoseLife();
            ResetBucket();
        }

        // Arrastrar el balde con el mouse
        if (isDragging)
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x + offset.x, mousePosition.y + offset.y, transform.position.z);
            rb.gravityScale = 0;  // Desactivar gravedad mientras se arrastra
            rb.velocity = Vector2.zero;
        }

        // Teletransportar el balde al Mundo Bueno si se presiona la tecla 'Q' y est� lleno
        if (isFilled && isDragging && Input.GetKeyDown(KeyCode.Q))
        {
            TeleportBucket();
        }
    }

    private void OnMouseDown()
    {
        Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        offset = transform.position - mousePosition;
        isDragging = true;
        rb.gravityScale = 0;  // Desactivar gravedad al agarrarlo
        rb.velocity = Vector2.zero;  // Detener el movimiento al agarrarlo
    }

    private void OnMouseUp()
    {
        isDragging = false;
        rb.gravityScale = 2;  // Activar gravedad al soltar el balde
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Water"))
        {
            isFilling = true;
            spriteRenderer.enabled = false;  // Ocultar el balde al sumergirlo
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Water"))
        {
            isFilling = false;
            spriteRenderer.enabled = true;  // Mostrar el balde al sacarlo del agua
        }
    }

    private void LoseLife()
    {
        if (livesRemaining > 0)
        {
            livesRemaining--;
            lifeSprites[livesRemaining].SetActive(false);  // Desactivar sprite de vida

            if (livesRemaining <= 0)
            {
                Debug.Log("Game Over");
                // Aqu� puedes activar un panel de "Juego Terminado"
            }
        }
    }

    private void ResetBucket()
    {
        transform.position = startPosition;  // Reiniciar balde a la posici�n inicial
        rb.velocity = Vector2.zero;  // Detener cualquier movimiento
        rb.gravityScale = 0;  // Desactivar gravedad hasta que se agarre de nuevo
        spriteRenderer.sprite = emptyBucketSprite;  // Cambiar a balde vac�o
        waterBar.value = 0;  // Resetear la barra de agua
        isFilled = false;  // El balde ya no est� lleno
    }

    private void TeleportBucket()
    {
        // Verificar que el balde est� lleno antes de teletransportarlo
        if (isFilled)
        {
            // Desactivar el balde vac�o en el Mundo Malo
            gameObject.SetActive(false);

            // Activar el balde lleno en el Mundo Bueno
            filledBucketInWorldGood.SetActive(true);

            // Teletransportar el balde al Mundo Bueno
            filledBucketInWorldGood.transform.position = teleportPosition;

            Debug.Log("Balde teletransportado al Mundo Bueno.");
        }
        else
        {
            Debug.Log("El balde no est� lleno.");
        }
    }

    private void UpdateWaterPercentageText()
    {
        float percentage = (waterBar.value / waterBar.maxValue) * 100;
        waterPercentageText.text = $"{percentage:F0}";
    }
}
