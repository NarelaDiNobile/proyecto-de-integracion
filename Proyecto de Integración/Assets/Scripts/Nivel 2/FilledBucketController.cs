using UnityEngine;

public class FilledBucketController : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;

    public GameObject[] lifeSprites;  // Array de sprites de vidas
    private int livesRemaining;

    public float fallLimit = -13f;  // L�mite de ca�da en Y donde se pierde una vida
    public Vector3 startPosition;   // Posici�n inicial del balde

    public Vector3 teleportPosition;  // Posici�n de teletransporte en el Mundo Bueno (a asignar desde el Inspector)

    private Camera mainCamera;      // Para capturar el movimiento del rat�n
    private Vector3 offset;         // Para arrastrar el balde con el rat�n
    private bool isDragging = false; // Para verificar si el balde est� siendo arrastrado

    public Animator bucketAnimator;  // Asigna el Animator del balde desde el Inspector
    public GameObject waterEffect;  // El GameObject que contiene el agua

    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;  // Capturar la c�mara principal
        startPosition = transform.position;  // Guardar la posici�n inicial
        livesRemaining = lifeSprites.Length;  // Asignar el n�mero de vidas inicial

        // Asegurarse de que el agua est� desactivada al inicio
        if (waterEffect != null)
        {
            waterEffect.SetActive(false);
        }
    }

    void Update()
    {
        // Si el balde ha ca�do por debajo del l�mite, perder una vida y reiniciar
        if (transform.position.y <= fallLimit && livesRemaining > 0)
        {
            LoseLife();  // Restar solo una vida
            ResetBucket();  // Resetear el balde a su posici�n inicial
        }

        // Si el balde est� siendo arrastrado
        if (isDragging)
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x + offset.x, mousePosition.y + offset.y, transform.position.z);
            rb.gravityScale = 0;  // Desactivar gravedad mientras se arrastra
        }

        // Control de la animaci�n con la tecla 'E'
        if (Input.GetKeyDown(KeyCode.E))
        {
            // Activar el agua y la animaci�n (rotaci�n del balde y ca�da del agua)
            if (waterEffect != null)
            {
                waterEffect.SetActive(true);
            }

            bucketAnimator.SetBool("IsPouring", true);
        }
        else if (Input.GetKeyUp(KeyCode.E))
        {
            // Detener las animaciones y desactivar el agua
            bucketAnimator.SetBool("IsPouring", false);
            if (waterEffect != null)
            {
                waterEffect.SetActive(false);
            }
        }
    }

    private void OnMouseDown()
    {
        // Al presionar el rat�n sobre el balde, habilitar el arrastre
        Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        offset = transform.position - mousePosition;
        isDragging = true;  // Iniciar el arrastre
        rb.gravityScale = 0;  // Desactivar la gravedad mientras lo arrastras
    }

    private void OnMouseUp()
    {
        // Cuando el jugador suelta el balde, se reactivar� la gravedad
        isDragging = false;  // Detener el arrastre
        rb.gravityScale = 2;  // Rehabilitar la gravedad
    }

    private void LoseLife()
    {
        if (livesRemaining > 0)
        {
            livesRemaining--;
            lifeSprites[livesRemaining].SetActive(false);  // Desactivar sprite de vida

            if (livesRemaining <= 0)
            {
                Debug.Log("Game Over");
                // Aqu� puedes activar un panel de "Juego Terminado"
            }
        }
    }

    private void ResetBucket()
    {
        // Reiniciar el balde a la posici�n inicial
        transform.position = startPosition;  // Reiniciar a la posici�n del balde lleno
        rb.velocity = Vector2.zero;  // Detener cualquier movimiento
        rb.gravityScale = 0;  // Desactivar gravedad temporalmente
        spriteRenderer.enabled = true;  // Asegurarse de que el balde est� visible
    }

    // M�todo para teletransportar el balde al Mundo Bueno
    private void TeleportBucket()
    {
        if (livesRemaining > 0)
        {
            // Cambiar la posici�n del balde y asegurarse de que se mantenga arrastrable
            transform.position = teleportPosition;
            Debug.Log("Balde teletransportado.");
        }
    }
}
