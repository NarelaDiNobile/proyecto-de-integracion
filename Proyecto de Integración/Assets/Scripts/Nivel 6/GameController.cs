using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public TMP_Text countdownText;
    public TMP_InputField resultInput;
    public Button verifyButton, easyButton, mediumButton, hardButton, difficultyButton;
    public GameObject gameOverPanel;
    public TMP_Text timerCountdownText;
    public GameObject[] numberObjects;
    public GameObject[] lifeSprites;

    public GameObject redDoor, blueDoor;
    public TMP_Text redDoorText, blueDoorText;

    private int totalLives = 4;
    private int currentLives;
    private int correctAnswer;
    private bool gameStarted = false;

    public bool redDoorTouched = true;
    public bool blueDoorTouched = true;

    private Button lastPlayedButton; // Bot�n de la �ltima dificultad jugada

    private Coroutine timerCoroutine;

    public TMP_Text levelHintText;

    public TMP_Text mensajePuertaRoja;
    public TMP_Text mensajePuertaAzul;

    public bool puertaRojaDesbloqueada = true;
    public bool puertaAzulDesbloqueada = true;

    public bool puertaRojaTocada = true;
    public bool puertaAzulTocada = true;

    public TMP_Text sumaText;

    public static NivelController instance;
    [SerializeField] Animator transitionAnim;
    public void Start()
    {
        currentLives = totalLives;

        resultInput.interactable = false;
        verifyButton.interactable = false;

        easyButton.onClick.AddListener(StartEasy);
        mediumButton.onClick.AddListener(StartMedium);
        hardButton.onClick.AddListener(StartHard);
        mediumButton.interactable = true;
        hardButton.interactable = true;
        verifyButton.onClick.AddListener(CheckAnswer);
        resultInput.onValueChanged.AddListener(EnableVerifyButton);

        foreach (var number in numberObjects)
        {
            number.SetActive(false);
        }

        // Configuraci�n inicial de las puertas
        redDoor.SetActive(true);
        blueDoor.SetActive(true);
        redDoorText.gameObject.SetActive(true);
        blueDoorText.gameObject.SetActive(true);

        // Aseg�rate de que el bot�n de dificultad tambi�n est� visible
        difficultyButton.gameObject.SetActive(true);
    }

    void Update()
    {
        // Si el clic del mouse est� sobre la puerta roja
        if (Input.GetMouseButtonDown(0)) // Bot�n izquierdo del mouse
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == redDoor && puertaRojaDesbloqueada)
                {
                    ColisionarConPuerta5("Roja");
                }
                else if (hit.collider.gameObject == blueDoor && puertaAzulDesbloqueada)
                {
                    ColisionarConPuerta5("Azul");
                }
            }
        }
    }

    private void StartEasy()
    {
        HideDifficultyButtons();
        lastPlayedButton = easyButton;  // Guardamos el bot�n presionado
        levelHintText.gameObject.SetActive(false);  // Desactivar el texto de nivel al iniciar una nueva dificultad
        StartCoroutine(ShowNumbersAndQuestion(3)); // Mostrar 3 n�meros en la dificultad f�cil
    }

    private void StartMedium()
    {
        HideDifficultyButtons();
        lastPlayedButton = mediumButton;  // Guardamos el bot�n presionado
        levelHintText.gameObject.SetActive(false);  // Desactivar el texto de nivel al iniciar una nueva dificultad
        StartCoroutine(ShowNumbersAndQuestionMedium());
    }

    private void StartHard()
    {
        HideDifficultyButtons();
        lastPlayedButton = hardButton;  // Guardamos el bot�n presionado
        levelHintText.gameObject.SetActive(false);  // Desactivar el texto de nivel al iniciar una nueva dificultad
        StartCoroutine(ShowNumbersAndQuestionHard());
    }

    private IEnumerator ShowNumbersAndQuestionMedium()
    {
        // Activar el texto de "SUMA"
        sumaText.gameObject.SetActive(true);

        countdownText.gameObject.SetActive(true);
        string[] countdown = new string[] { "Preparados", "Listos", "Ya" };

        foreach (string word in countdown)
        {
            countdownText.text = word;
            yield return new WaitForSeconds(1);
        }

        countdownText.gameObject.SetActive(false);

        int[] selectedNumbers = new int[5];
        correctAnswer = 0;

        for (int i = 0; i < selectedNumbers.Length; i++)
        {
            int randomIndex = Random.Range(0, numberObjects.Length);
            selectedNumbers[i] = randomIndex + 1;
            correctAnswer += selectedNumbers[i];

            numberObjects[randomIndex].SetActive(true);
            yield return new WaitForSeconds(3);
            numberObjects[randomIndex].SetActive(false);
        }

        // Desactivar el texto "SUMA"
        sumaText.gameObject.SetActive(false);

        // Iniciar el temporizador de 20 segundos
        StartCoroutine(StartTimer());

        resultInput.interactable = true;
        verifyButton.interactable = false;

        resultInput.onValueChanged.AddListener(EnableVerifyButton);
    }

    private IEnumerator StartTimer()
    {
        gameStarted = true; // Ahora el juego ha comenzado
        float timer = 20f; // 20 segundos de tiempo

        while (timer > 0)
        {
            timerCountdownText.gameObject.SetActive(true);

            timerCountdownText.text = Mathf.Ceil(timer).ToString(); // Mostrar el tiempo restante
            timer -= Time.deltaTime; // Reducir el tiempo
            yield return null;

            // Si el juego se detuvo, salir del temporizador
            if (!gameStarted) yield break;
        }

        // Si el tiempo llega a 0 y el juego sigue activo, perder una vida y reiniciar el contador
        if (gameStarted)
        {
            LoseLife();
        }
    }


    private IEnumerator ShowNumbersAndQuestionHard()
    {
        // Activar el texto de "SUMA"
        sumaText.gameObject.SetActive(true);

        countdownText.gameObject.SetActive(true);
        string[] countdown = new string[] { "Preparados", "Listos", "Ya" };

        foreach (string word in countdown)
        {
            countdownText.text = word;
            yield return new WaitForSeconds(1);
        }

        countdownText.gameObject.SetActive(false);

        int[] selectedNumbers = new int[7];  // 7 n�meros para dificultad dif�cil
        correctAnswer = 0;

        for (int i = 0; i < selectedNumbers.Length; i++)
        {
            selectedNumbers[i] = Random.Range(1, 11);
            correctAnswer += selectedNumbers[i];

            // Aseguramos que el n�mero correspondiente se active
            numberObjects[selectedNumbers[i] - 1].SetActive(true);
            yield return new WaitForSeconds(3);

            // Aseguramos que el n�mero se desactive despu�s de 3 segundos
            numberObjects[selectedNumbers[i] - 1].SetActive(false);
        }

        // Desactivar el texto "SUMA"
        sumaText.gameObject.SetActive(false);

        // Desactiva el bot�n de verificar y el input inmediatamente
        resultInput.interactable = false;
        verifyButton.interactable = false;

        // Iniciar el temporizador de 20 segundos
        StartCoroutine(StartTimer());
        // Habilitar los controles despu�s de que termine la visualizaci�n
        resultInput.interactable = true;
        verifyButton.interactable = true;
        resultInput.onValueChanged.AddListener(EnableVerifyButton);
    }

    private void HideDifficultyButtons()
    {
        // Guardar el bot�n que se us�
        if (easyButton.gameObject.activeSelf) lastPlayedButton = easyButton;
        if (mediumButton.gameObject.activeSelf) lastPlayedButton = mediumButton;
        if (hardButton.gameObject.activeSelf) lastPlayedButton = hardButton;

        easyButton.gameObject.SetActive(false);
        mediumButton.gameObject.SetActive(false);
        hardButton.gameObject.SetActive(false);
        difficultyButton.gameObject.SetActive(false);

        countdownText.gameObject.SetActive(true);
        resultInput.gameObject.SetActive(true);
        resultInput.interactable = false;
        verifyButton.interactable = false;
    }

    private IEnumerator ShowNumbersAndQuestion(int cantidadDeNumeros)
    {
        // Activar el texto de "SUMA" cuando empiece la secuencia
        sumaText.gameObject.SetActive(true);

        string[] countdown = { "Preparados", "Listos", "Ya" };
        foreach (string word in countdown)
        {
            countdownText.text = word;
            yield return new WaitForSeconds(1);
        }

        countdownText.gameObject.SetActive(false);

        int[] selectedNumbers = new int[cantidadDeNumeros];
        correctAnswer = 0;

        for (int i = 0; i < cantidadDeNumeros; i++)
        {
            int randomIndex = Random.Range(0, numberObjects.Length); // Asegurar que se obtiene un �ndice v�lido
            selectedNumbers[i] = randomIndex + 1;  // Guardar el n�mero (1-10)
            correctAnswer += selectedNumbers[i];

            // Mostrar el n�mero correspondiente
            GameObject numObj = numberObjects[randomIndex];
            numObj.SetActive(true);

            yield return new WaitForSeconds(3); // Esperar 3 segundos

            // Ocultar el n�mero despu�s del tiempo
            numObj.SetActive(false);
        }

        // Desactivar el texto "SUMA" una vez que empiece el temporizador
        sumaText.gameObject.SetActive(false);

        // Iniciar el temporizador de 20 segundos
        timerCoroutine = StartCoroutine(StartTimer());

        // Activar input para la respuesta
        resultInput.interactable = true;
        verifyButton.interactable = false;
        resultInput.onValueChanged.AddListener(EnableVerifyButton);
    }


    public void CheckAnswer()
    {
        // Desactivar el texto del temporizador
        timerCountdownText.gameObject.SetActive(false);

        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
            timerCoroutine = null;
        }

        gameStarted = false;

        // Desactivar el texto "SUMA" cuando se verifica la respuesta
        sumaText.gameObject.SetActive(false);

        if (int.TryParse(resultInput.text, out int playerAnswer))
        {
            if (playerAnswer == correctAnswer)
            {
                resultInput.textComponent.color = Color.green;

                // Reactivar los botones de dificultad
                easyButton.gameObject.SetActive(true);
                mediumButton.gameObject.SetActive(true);
                hardButton.gameObject.SetActive(true);
                difficultyButton.gameObject.SetActive(true);

                // Bloquear el bot�n de la dificultad jugada
                if (lastPlayedButton == easyButton)
                    easyButton.interactable = false;
                else if (lastPlayedButton == mediumButton)
                    mediumButton.interactable = false;
                else if (lastPlayedButton == hardButton)
                    hardButton.interactable = false;

                // Activar las puertas y textos
                redDoorText.gameObject.SetActive(true);
                blueDoorText.gameObject.SetActive(true);
                redDoor.GetComponent<BoxCollider2D>().enabled = true;
                blueDoor.GetComponent<BoxCollider2D>().enabled = true;

                redDoorText.text = "TOCA LA PUERTA ROJA";
                blueDoorText.text = "TOCA LA PUERTA AZUL";

                StartCoroutine(ClearInputField());
            }
            else
            {
                resultInput.textComponent.color = Color.red;
                StartCoroutine(ClearInputFieldIncorrect());
                LoseLife();
            }
        }
    }


    private IEnumerator ClearInputField()
    {
        yield return new WaitForSeconds(2);
        resultInput.text = "";
        resultInput.textComponent.color = Color.white;  // Asegura que el color del input se resetea a blanco
    }

    private IEnumerator ClearInputFieldIncorrect()
    {
        yield return new WaitForSeconds(1);
        resultInput.text = "";
        resultInput.textComponent.color = Color.white;  // Asegura que el color del input se resetea a blanco
    }

    private void LoseLife()
    {
        currentLives--;

        if (currentLives >= 0 && currentLives < lifeSprites.Length)
        {
            lifeSprites[currentLives].SetActive(false);
        }

        // Desactivar el texto del temporizador
        timerCountdownText.gameObject.SetActive(false);

        // Detener el temporizador si est� en ejecuci�n
        if (timerCoroutine != null)
        {
            StopCoroutine(timerCoroutine);
            timerCoroutine = null; // Resetear la referencia
        }

        gameStarted = false; // Detener el juego correctamente


        if (currentLives <= 0)
        {
            gameOverPanel.SetActive(true);
        }
        else
        {
            countdownText.gameObject.SetActive(true);

            // Evitar que se interrumpan corutinas de n�meros
            StopAllCoroutines(); // Se reinician correctamente antes de empezar una nueva dificultad

            if (lastPlayedButton == easyButton)
            {
                StartCoroutine(ShowNumbersAndQuestion(3)); // 3 n�meros para f�cil
            }
            else if (lastPlayedButton == mediumButton)
            {
                StartCoroutine(ShowNumbersAndQuestionMedium()); // 5 n�meros para medio
            }
            else if (lastPlayedButton == hardButton)
            {
                StartCoroutine(ShowNumbersAndQuestionHard()); // 7 n�meros para dif�cil
            }
        }
    }

    private void EnableVerifyButton(string inputText)
    {
        if (resultInput.interactable)
        {
            verifyButton.interactable = !string.IsNullOrEmpty(inputText);
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitGame()
    {
        SceneManager.LoadScene("Menu");
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void ColisionarConPuerta5(string puerta)
    {
        Debug.Log("Se toc� la puerta: " + puerta);  // Aseg�rate de que se detecta el clic

        if (puerta == "Roja" && puertaRojaDesbloqueada)
        {
            puertaRojaTocada = true;
            mensajePuertaRoja.text = "�Puerta Roja tocada!";
        }
        else if (puerta == "Azul" && puertaAzulDesbloqueada)
        {
            puertaAzulTocada = true;
            mensajePuertaAzul.text = "�Puerta Azul tocada!";
        }

        // Comprobar si ambas puertas han sido tocadas
        ComprobarYAvanzar();
    }


    public void ComprobarYAvanzar()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if(puertaRojaTocada && puertaAzulTocada)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}





