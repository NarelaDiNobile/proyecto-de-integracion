using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NivelController4 : MonoBehaviour
{
    public GameObject panelGameOver;
    public int vidasRestantes = 4; // Iniciar con 4 vidas
    public GameObject[] lifeSprites;

    public static NivelController4 Instance;

    private const string VidasKey = "VidasRestantes"; // Clave para guardar las vidas en PlayerPrefs

    public Button reiniciarButton;
    public Button menuButton;
    public Button salirButton;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        // Cargar las vidas guardadas al iniciar
        vidasRestantes = PlayerPrefs.GetInt(VidasKey, 4); // Si no hay valor guardado, pone 4 por defecto
        ActualizarSpritesVidas();  // Actualizar los sprites de las vidas
    }

    public void PerderVida()
    {
        if (vidasRestantes > 0)
        {
            vidasRestantes--;  // Restamos una vida
            ActualizarSpritesVidas();  // Actualizamos los sprites de vida

            // Guardamos el n�mero de vidas restantes
            PlayerPrefs.SetInt(VidasKey, vidasRestantes);

            if (vidasRestantes <= 0)
            {
                ActivarGameOver();
            }
        }
    }

    private void ActivarGameOver()
    {
        panelGameOver.SetActive(true);
        Time.timeScale = 0;  // Pausar el juego
    }

    public void ReiniciarNivel()
    {
        // Cuando se reinicia desde el Game Over, restablecer las vidas a 4
        vidasRestantes = 4;
        PlayerPrefs.SetInt(VidasKey, vidasRestantes);  // Guardar el valor de 4 vidas
        ActualizarSpritesVidas();  // Actualizamos los sprites de vida
        Time.timeScale = 1;

        // Reiniciar la escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void IrAlMenu()
    {
        // Reiniciar el contador de vidas y volver al men� principal
        vidasRestantes = 4;
        PlayerPrefs.SetInt(VidasKey, vidasRestantes);  // Guardar el valor de 4 vidas
        Time.timeScale = 1;

        // Cargar la escena del men�
        SceneManager.LoadScene("Menu");
    }

    public void SalirJuego()
    {
        // Salir de la aplicaci�n
        Application.Quit();
    }

    private void ActualizarSpritesVidas()
    {
        // Actualizamos los sprites de las vidas
        for (int i = 0; i < lifeSprites.Length; i++)
        {
            if (i < vidasRestantes)
                lifeSprites[i].SetActive(true);  // Activar sprite de vida
            else
                lifeSprites[i].SetActive(false); // Desactivar sprite de vida
        }
    }

    private void Start()
    {
        // Asignar los listeners a los botones del panel de Game Over
        reiniciarButton.onClick.AddListener(ReiniciarNivel);
        menuButton.onClick.AddListener(IrAlMenu);
        salirButton.onClick.AddListener(SalirJuego);

        // Actualizar los sprites de las vidas al inicio
        ActualizarSpritesVidas();
    }

    private void OnApplicationQuit()
    {
        // Guardar las vidas cuando el juego se cierra
        PlayerPrefs.SetInt(VidasKey, vidasRestantes);
    }
}
