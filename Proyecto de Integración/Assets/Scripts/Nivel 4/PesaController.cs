using UnityEngine;

public class PesaController : MonoBehaviour
{
    public int peso;  // Valor de la pesa
    public bool isInMedidor = false;  // Controlar si la pesa ya se registr� en el medidor
    private Rigidbody2D rb;
    private DragPesas dragPesas;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        dragPesas = GetComponent<DragPesas>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("MedidorPeso") && !isInMedidor)
        {
            isInMedidor = true; // Marcar la pesa como registrada
            MedidorController.Instance.AgregarPeso(peso);
        }
    }
}
 

