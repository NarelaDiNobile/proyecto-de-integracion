using UnityEngine;

public class Puerta4 : MonoBehaviour
{
    public string tipoDePuerta; // Puede ser "Roja" o "Azul"

    private void OnMouseDown()
    {
        // Solo detecta el clic si la puerta est� desbloqueada
        if (PuertasController.Instance.PuertaDesbloqueada(tipoDePuerta))
        {
            PuertasController.Instance.ColisionarConPuerta(tipoDePuerta); // Notifica al controlador que la puerta fue tocada
        }
    }
}

