using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MedidorController : MonoBehaviour
{
    public static MedidorController Instance;

    public int pesoTotal = 0;
    public int pesoObjetivo = 124;
    public TMP_Text textoPeso;
    public TMP_Text mensaje;
    public TMP_Text mensajePuertaRoja;
    public TMP_Text mensajePuertaAzul;
    public GameObject panelGameOver;

    public Button verificarButton;
    public Button reiniciarButton;

    private bool yaVerificado = false; // Evitar doble verificaci�n

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }

    private void Start()
    {
        verificarButton.onClick.AddListener(VerificarPeso);
        reiniciarButton.onClick.AddListener(ReiniciarNivel);

        OcultarPeso();
    }

    public void AgregarPeso(int peso)
    {
        pesoTotal += peso;
        OcultarPeso();
    }

    private void OcultarPeso()
    {
        textoPeso.text = $"Peso actual: ??? kg / {pesoObjetivo} kg";
    }

    private void MostrarPeso()
    {
        textoPeso.text = $"Peso actual: {pesoTotal} kg / {pesoObjetivo} kg";
    }

    public void VerificarPeso()
    {
        if (yaVerificado) return; // Evita que la funci�n se ejecute dos veces seguidas
        yaVerificado = true;

        MostrarPeso();  // Mostrar el peso real al presionar el bot�n

        if (pesoTotal == pesoObjetivo)
        {
            mensaje.text = "�Peso correcto!";
            // Activamos los textos de "Puerta desbloqueada" para ambas puertas
            mensajePuertaRoja.text = "�Puerta desbloqueada!";
            mensajePuertaAzul.text = "�Puerta desbloqueada!";

            // Desbloqueamos las puertas para que puedan ser tocadas
            PuertasController.Instance.DesbloquearPuertas();
        }
        else
        {
            mensaje.text = "Peso incorrecto";
            NivelController4.Instance.PerderVida();  // Solo resta una vida
        }

        // Permitir volver a verificar solo despu�s de reiniciar
        Invoke(nameof(ResetVerificacion), 0.1f);
    }

    private void ResetVerificacion()
    {
        yaVerificado = false;
    }

    public void ReiniciarNivel()
    {
        // No restablecer las vidas, solo reiniciar la escena
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
