using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;

public class PuertasController : MonoBehaviour
{
    public static PuertasController Instance;

    public GameObject puertaRoja;
    public GameObject puertaAzul;
    public TMP_Text mensajePuertaRoja;
    public TMP_Text mensajePuertaAzul;

    private bool puertaRojaDesbloqueada = false;
    private bool puertaAzulDesbloqueada = false;

    private bool puertaRojaTocada = false;
    private bool puertaAzulTocada = false;

    public static PuertasController instance;
    [SerializeField] Animator transitionAnim;

    private void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(gameObject);
    }

    // Desbloquea las puertas cuando el peso es correcto
    public void DesbloquearPuertas()
    {
        puertaRojaDesbloqueada = true;
        puertaAzulDesbloqueada = true;
        // Mostrar mensaje de puertas desbloqueadas
        mensajePuertaRoja.text = "�Puerta desbloqueada!";
        mensajePuertaAzul.text = "�Puerta desbloqueada!";
        // Aqu� activamos las puertas para que puedan ser tocadas
        puertaRoja.SetActive(true);
        puertaAzul.SetActive(true);
        Debug.Log("Puertas desbloqueadas");
    }

    // Verifica si la puerta est� desbloqueada
    public bool PuertaDesbloqueada(string tipoDePuerta)
    {
        if (tipoDePuerta == "Roja") return puertaRojaDesbloqueada;
        if (tipoDePuerta == "Azul") return puertaAzulDesbloqueada;
        return false;
    }

    // L�gica para detectar cuando las puertas son tocadas
    public void ColisionarConPuerta(string puerta)
    {
        Debug.Log("Se toc� la puerta: " + puerta);  // Aseg�rate de que se detecta el clic

        if (puerta == "Roja" && puertaRojaDesbloqueada)
        {
            puertaRojaTocada = true;
            mensajePuertaRoja.text = "�Puerta Roja tocada!";
        }
        else if (puerta == "Azul" && puertaAzulDesbloqueada)
        {
            puertaAzulTocada = true;
            mensajePuertaAzul.text = "�Puerta Azul tocada!";
        }

        // Comprobar si ambas puertas han sido tocadas
        ComprobarYAvanzar();
    }

    private void ComprobarYAvanzar()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if (puertaRojaTocada && puertaAzulTocada)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}
