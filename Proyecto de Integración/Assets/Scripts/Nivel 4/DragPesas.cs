using UnityEngine;

public class DragPesas : MonoBehaviour
{
    private Vector3 offset;  // Desplazamiento entre la posici�n del mouse y la pesa
    private bool isDragging = false;  // Si se est� arrastrando la pesa

    private void OnMouseDown()
    {
        // Al hacer clic en la pesa, comenzamos a arrastrarla
        isDragging = true;

        // Guardar el offset entre la posici�n de la pesa y el mouse
        offset = transform.position - GetMouseWorldPos();
    }

    private void OnMouseUp()
    {
        // Al soltar el mouse, dejamos de arrastrar
        isDragging = false;
    }

    private void Update()
    {
        if (isDragging)
        {
            // Mover la pesa seg�n la posici�n del mouse
            transform.position = GetMouseWorldPos() + offset;
        }
    }

    private Vector3 GetMouseWorldPos()
    {
        // Obtener la posici�n del mouse en el mundo, considerando la c�mara
        return Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }
}
