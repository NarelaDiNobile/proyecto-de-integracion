using UnityEngine;
using UnityEngine.SceneManagement;  // Para cambiar de escena

public class GameOverManager : MonoBehaviour
{
    public GameObject gameOverPanel;  // Panel de Game Over

    public void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);  // Activar el panel
    }

    public void RestartGame()
    {
        // Reiniciar la escena actual
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void GoToMenu()
    {
        // Aqu� puedes cargar el men� principal (aseg�rate de que la escena del men� est� configurada en Build Settings)
        SceneManager.LoadScene("Menu");  // Cambia "Menu" por el nombre real de tu escena de men�
    }

    public void ExitGame()
    {
        // Salir del juego
        Application.Quit();
    }
}
