using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;  // Para cambiar de escena
using System.Collections;

public class PositionChecker : MonoBehaviour
{
    public Transform[] correctNumberPanels;
    public Transform[] correctSymbolPanels;
    public GameObject[] numbers;
    public GameObject[] symbols;
    public float tolerance = 0.5f;
    public Color correctColor = Color.green;
    public Color incorrectColor = Color.red;
    public TMP_Text correctAnswersText;
    public Button verifyButton;
    public Button resetButton;
    public TMP_Text timerText;  // Texto para mostrar el tiempo
    public GameObject gameOverPanel;  // Panel de Game Over
    public TMP_Text blueDoorText;  // Texto para puerta azul
    public TMP_Text redDoorText;   // Texto para puerta roja
    public GameObject Azul;    // Puerta azul (Sprite)
    public GameObject Roja;     // Puerta roja (Sprite)

    private int totalPanels = 14;
    private float timer = 120f;  // 2 minutos en segundos
    private bool isPaused = false;  // Para pausar el contador cuando se complete el juego
    private bool blueDoorClicked = false;
    private bool redDoorClicked = false;

    public static PositionChecker instance;
    [SerializeField] Animator transitionAnim;

    void Start()
    {
        verifyButton.interactable = false;
        resetButton.interactable = false;
        blueDoorText.gameObject.SetActive(false);
        redDoorText.gameObject.SetActive(false);
        gameOverPanel.SetActive(false);
    }

    void Update()
    {
        // Solo contamos el tiempo si el juego no est� pausado
        if (!isPaused)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                timer = 0;
                gameOverPanel.SetActive(true); // Muestra el panel de Game Over
                isPaused = true;
            }

            // Convertir tiempo a minutos y segundos
            int minutes = Mathf.FloorToInt(timer / 60);
            int seconds = Mathf.FloorToInt(timer % 60);
            timerText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
        }

        // Verificar si todos los paneles est�n ocupados
        CheckIfPanelsAreFilled();

        // Detectar clics en las puertas
        DetectDoorClick();
    }

    void CheckIfPanelsAreFilled()
    {
        int filledCount = 0;

        foreach (var num in numbers)
        {
            if (num.transform.position != num.GetComponent<DragAndDrop5>().startPosition)
                filledCount++;
        }

        foreach (var sym in symbols)
        {
            if (sym.transform.position != sym.GetComponent<DragAndDrop5>().startPosition)
                filledCount++;
        }

        if (filledCount == totalPanels)
        {
            verifyButton.interactable = true;
        }
        else
        {
            verifyButton.interactable = false;
        }
    }

    public void VerifyAnswers()
    {
        int correctCount = 0;

        for (int i = 0; i < numbers.Length; i++)
        {
            if (Vector2.Distance(numbers[i].transform.position, correctNumberPanels[i].position) < tolerance)
            {
                correctCount++;
                correctNumberPanels[i].GetComponent<SpriteRenderer>().color = correctColor;
            }
            else
            {
                correctNumberPanels[i].GetComponent<SpriteRenderer>().color = incorrectColor;
            }
        }

        for (int i = 0; i < symbols.Length; i++)
        {
            if (Vector2.Distance(symbols[i].transform.position, correctSymbolPanels[i].position) < tolerance)
            {
                correctCount++;
                correctSymbolPanels[i].GetComponent<SpriteRenderer>().color = correctColor;
            }
            else
            {
                correctSymbolPanels[i].GetComponent<SpriteRenderer>().color = incorrectColor;
            }
        }

        correctAnswersText.text = "Correctas: " + correctCount + " de " + totalPanels;

        if (correctCount == totalPanels)
        {
            isPaused = true;
            blueDoorText.gameObject.SetActive(true);
            redDoorText.gameObject.SetActive(true);
        }

        resetButton.interactable = true;
    }

    public void ResetIncorrects()
    {
        for (int i = 0; i < numbers.Length; i++)
        {
            if (Vector2.Distance(numbers[i].transform.position, correctNumberPanels[i].position) > tolerance)
            {
                numbers[i].transform.position = numbers[i].GetComponent<DragAndDrop5>().startPosition;
            }
        }

        for (int i = 0; i < symbols.Length; i++)
        {
            if (Vector2.Distance(symbols[i].transform.position, correctSymbolPanels[i].position) > tolerance)
            {
                symbols[i].transform.position = symbols[i].GetComponent<DragAndDrop5>().startPosition;
            }
        }
    }

    // Detectar colisi�n con el mouse en las puertas
    void DetectDoorClick()
    {
        if (Input.GetMouseButtonDown(0)) // Clic izquierdo
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(mousePosition, Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == Azul)
                {
                    blueDoorClicked = true;
                    blueDoorText.color = Color.green; // Indicar que fue tocada
                }
                if (hit.collider.gameObject == Roja)
                {
                    redDoorClicked = true;
                    redDoorText.color = Color.green; // Indicar que fue tocada
                }

                CheckDoors();
            }
        }
    }

    // Verificar si ambas puertas han sido tocadas
    void CheckDoors()
    {
        if (blueDoorClicked && redDoorClicked)
        {
            LoadNextScene();
        }
    }

    // Cargar la siguiente escena
    void LoadNextScene()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        transitionAnim.SetTrigger("End");
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        transitionAnim.SetTrigger("Start");

    }
}
