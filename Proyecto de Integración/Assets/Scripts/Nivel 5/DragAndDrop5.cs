using UnityEngine;

public class DragAndDrop5 : MonoBehaviour
{
    public Vector3 startPosition;
    private bool isDragging = false;

    private void OnMouseDown()
    {
        startPosition = transform.position;
        isDragging = true;
    }

    private void OnMouseDrag()
    {
        if (isDragging)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePos.x, mousePos.y, 0);
        }
    }

    private void OnMouseUp()
    {
        isDragging = false;
    }

    public void ResetPosition()
    {
        transform.position = startPosition;
    }
}

