using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DetectorMicrofono : MonoBehaviour
{
    public Slider sliderMicrofono;  // Barra del micr�fono
    public Image barraColor;  // Color de la barra
    public float umbralAdvertencia = 0.05f;  // Advertencia (amarillo)
    public float umbralGameOver = 0.1f;  // Game Over (rojo)
    public GameObject panelGameOver;  // Panel de Game Over

    private AudioClip micInput;
    private bool detectandoSonido = false;
    private bool enProcesoGameOver = false; // Para evitar m�ltiples activaciones

    void Start()
    {
        panelGameOver.SetActive(false);

        if (Microphone.devices.Length > 0)
        {
            micInput = Microphone.Start(null, true, 1, 44100);
            detectandoSonido = true;
        }
        else
        {
            Debug.LogWarning("No hay micr�fono disponible.");
        }
    }

    void Update()
    {
        if (detectandoSonido && !enProcesoGameOver)
        {
            float nivelSonido = ObtenerNivelMicrofono();
            sliderMicrofono.value = nivelSonido;

            // Cambiar color seg�n el nivel de sonido
            if (nivelSonido < umbralAdvertencia)
            {
                barraColor.color = Color.green;  // Verde = Seguro
            }
            else if (nivelSonido < umbralGameOver)
            {
                barraColor.color = Color.yellow;  // Amarillo = Advertencia
            }
            else if (!enProcesoGameOver) // Solo si a�n no est� en proceso el Game Over
            {
                StartCoroutine(EsperarYActivarGameOver());
            }
        }
    }

    float ObtenerNivelMicrofono()
    {
        if (micInput == null) return 0f;

        int sampleSize = 128;
        float[] waveData = new float[sampleSize];
        int micPos = Microphone.GetPosition(null) - (sampleSize + 1);
        if (micPos < 0) return 0f;

        micInput.GetData(waveData, micPos);
        float nivelMaximo = 0f;

        foreach (float sample in waveData)
        {
            float nivelAbs = Mathf.Abs(sample);
            if (nivelAbs > nivelMaximo)
            {
                nivelMaximo = nivelAbs;
            }
        }

        return nivelMaximo * 6f;
    }

    IEnumerator EsperarYActivarGameOver()
    {
        enProcesoGameOver = true;  // Evita m�ltiples activaciones
        barraColor.color = Color.red;  // Cambia a rojo inmediatamente
        yield return new WaitForSeconds(2f);  // Espera 2 segundos
        panelGameOver.SetActive(true);  // Activa el panel de Game Over
    }
}
