using UnityEngine;
using TMPro;
using System.Collections;

public class ColorManager : MonoBehaviour
{
    public SpriteRenderer sprite1;
    public SpriteRenderer sprite2;

    private Color[] secuenciaColores = new Color[8] {
        Color.red, Color.blue,
        Color.blue, Color.blue,
        Color.red, Color.blue,
        Color.blue, Color.red
    };

    private int indice = 0; // Para recorrer la secuencia

    void Start()
    {
        // Llamamos la corrutina que tiene el retraso antes de empezar
        StartCoroutine(CambiarColoresConTiempo());
    }

    IEnumerator CambiarColoresConTiempo()
    {
        // Esperar 2 segundos antes de empezar
        yield return new WaitForSeconds(3);

        // Iniciar el cambio de colores despu�s del retraso
        while (indice < secuenciaColores.Length - 2) // Se detiene en el �ltimo cambio
        {
            CambiarColores();
            yield return new WaitForSeconds(2); // Esperar 2 segundos entre cambios
            indice += 2; // Avanzar el �ndice en 2 para cambiar los dos sprites
        }
    }

    void CambiarColores()
    {
        sprite1.color = secuenciaColores[indice];
        sprite2.color = secuenciaColores[indice + 1];
    }
}
