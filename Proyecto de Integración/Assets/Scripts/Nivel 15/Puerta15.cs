using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class Puerta15 : MonoBehaviour
{
    public GameObject puertaAzul;
    public GameObject puertaRoja;
    public TMP_Text textoAzul;
    public TMP_Text textoRojo;

    private bool puertaAzulClickeada = false;
    private bool puertaRojaClickeada = false;

    private void Update()
    {
        // Detectamos el clic en la puerta azul
        if (Input.GetMouseButtonDown(0)) // 0 es el bot�n izquierdo del rat�n
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == puertaAzul)
                {
                    puertaAzulClickeada = true;
                    textoAzul.text = "�Puerta azul clickeada!";
                }
                else if (hit.collider.gameObject == puertaRoja)
                {
                    puertaRojaClickeada = true;
                    textoRojo.text = "�Puerta roja clickeada!";
                }
            }
        }

        // Comprobar si ambas puertas han sido clickeadas
        if (puertaAzulClickeada && puertaRojaClickeada)
        {
            CambiarDeNivel();
        }
    }

    private void CambiarDeNivel()
    {
        // Cambiar a la siguiente escena
        SceneManager.LoadScene("Nivel 16"); // Cambia "NivelSiguiente" por el nombre de tu siguiente nivel
    }
}
