using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Linterna : MonoBehaviour
{
    public GameObject panelNegro;  // El panel negro (sprite opaco)
    public GameObject linternaCircle;  // El c�rculo que representa la linterna
    public Light2D linternaLight;  // La luz 2D de la linterna (Point Light 2D)

    void Start()
    {
        // Asegurar que la linterna est� activa desde el inicio
        linternaLight.gameObject.SetActive(true);
        linternaCircle.SetActive(true);
    }

    void Update()
    {
        // Obtener la posici�n del mouse en el mundo 2D
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        // Mover el c�rculo de la linterna con el mouse
        linternaCircle.transform.position = new Vector3(mousePos.x, mousePos.y, linternaCircle.transform.position.z);

        // Mover la luz del Point Light 2D al mismo lugar del c�rculo
        linternaLight.transform.position = new Vector3(mousePos.x, mousePos.y, linternaLight.transform.position.z);
    }
}
