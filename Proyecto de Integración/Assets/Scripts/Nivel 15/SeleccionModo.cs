using UnityEngine;
using UnityEngine.UI;

public class SeleccionModo : MonoBehaviour
{
    public GameObject panelMicrofono;  // Panel con los botones SI y NO
    public Button botonSi;  // Bot�n "S�"
    public Button botonNo;  // Bot�n "No"

    public GameObject modoSilencio;  // Contenedor con todos los elementos del Modo Silencio
    public GameObject modoAcertijo;  // Contenedor con los elementos del Modo Acertijo

    void Start()
    {
        // Asegurar que el panel de selecci�n est� activo al inicio
        panelMicrofono.SetActive(true);

        // Desactivar ambos modos al inicio
        modoSilencio.SetActive(false);
        modoAcertijo.SetActive(false);

        // Asignar eventos a los botones
        botonSi.onClick.AddListener(ActivarModoSilencio);
        botonNo.onClick.AddListener(ActivarModoAcertijo);
    }

    void ActivarModoSilencio()
    {
        panelMicrofono.SetActive(false); // Ocultar el panel de selecci�n
        modoSilencio.SetActive(true); // Activar los elementos del Modo Silencio
        modoAcertijo.SetActive(false); // Desactivar el Modo Acertijo en caso de que estuviera activo
    }

    void ActivarModoAcertijo()
    {
        // Desactiva otros modos antes de activar el Modo Acertijo
        modoSilencio.SetActive(false); // Desactiva el modo silencio
        modoAcertijo.SetActive(true); // Activa el modo acertijo

        panelMicrofono.SetActive(false); // Ocultar el panel de selecci�n
    }

    // M�todo para reiniciar la selecci�n del modo
    public void ReiniciarSeleccion()
    {
        // Esto reinicia solo el modo silencio, sin mostrar el panel de micr�fono
        modoSilencio.SetActive(true);  // Activar el modo silencio
        modoAcertijo.SetActive(false); // Desactivar el modo acertijo

        // Aseg�rate de que el panel de selecci�n no se muestre
        panelMicrofono.SetActive(false);
    }
}
