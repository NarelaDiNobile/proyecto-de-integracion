using UnityEngine;

public class BotonManager : MonoBehaviour
{
    public static BotonManager Instance;

    // Referencias a los paneles y objetos en la UI
    public GameObject cartelMicrofono;
    public GameObject panelReiniciar;
    public GameObject panelGameOver;

    // Estado de selecci�n del bot�n
    public enum BotonSeleccionado { Ninguno, Acertijo, OtroModo }
    public BotonSeleccionado estadoBoton;

    private void Awake()
    {
        // Asegurarse de que haya una instancia �nica del BotonManager
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Este m�todo se activa cuando se presiona el bot�n de "Reiniciar"
    public void ReiniciarJuego()
    {
        // Activamos el cartel del micr�fono
        cartelMicrofono.SetActive(true);  // Muestra el cartel del micr�fono cuando se reinicia el juego

        // Restablecemos la l�gica del juego si es necesario
        if (estadoBoton == BotonSeleccionado.Ninguno)
        {
            // Si no hay ning�n modo seleccionado, podemos mostrar algo o reiniciar el estado de juego
            Debug.Log("Reiniciando el juego...");
            // Aqu� puedes agregar el c�digo necesario para restablecer otros elementos del juego
        }
        else
        {
            // Si se seleccion� alg�n modo de juego, lo reiniciamos seg�n sea necesario
            Debug.Log("Reiniciando en el modo seleccionado...");
            // Restablecer el modo de juego elegido
        }

        // Deja de mostrar el panel de Game Over y el panel de reiniciar
        panelGameOver.SetActive(false);
        panelReiniciar.SetActive(false);

        // Aqu� puedes agregar la l�gica de reinicio espec�fica de tu juego (por ejemplo, reiniciar el contador, etc.)
    }

    // Este m�todo se llama al presionar el bot�n "S�" de la pantalla de confirmaci�n
    public void ConfirmarReinicio()
    {
        ReiniciarJuego();  // Llama a la funci�n para reiniciar el juego
    }

    // Este m�todo se llama al presionar el bot�n "No" de la pantalla de confirmaci�n
    public void CancelarReinicio()
    {
        // Si se cancela, no hacemos nada o podemos cerrar el panel
        panelReiniciar.SetActive(false);
    }
}
