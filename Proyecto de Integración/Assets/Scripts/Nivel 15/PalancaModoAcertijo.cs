using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement; // Necesario para reiniciar la escena

public class PalancaModoAcertijo : MonoBehaviour
{
    private Animator animator;
    public bool esCorrecta; // Define si esta palanca es la correcta

    public GameObject puertas;  // Contenedor con las dos puertas y textos
    public TMP_Text textoTitulo;  // Texto principal que cambia si fallas
    public GameObject panelGameOver;  // Panel de Game Over
    public TMP_Text textoError;  // Texto de error: "�Te dije que no me despiertes!"

    private static bool palancaActivada = false;  // Estado global que indica si ya se activ� una palanca

    void Start()
    {
        // �IMPORTANTE! Restablecer la variable al reiniciar el juego
        palancaActivada = false;

        animator = GetComponent<Animator>();
        puertas.SetActive(false);  // Las puertas est�n desactivadas al inicio
        panelGameOver.SetActive(false);  // El panel de Game Over est� desactivado
    }

    void OnMouseDown()
    {
        if (palancaActivada) return;  // Si ya se ha activado una palanca, no hacer nada m�s

        animator.SetTrigger("Activar");

        // Marca que se ha activado una palanca
        palancaActivada = true;

        if (esCorrecta)
        {
            Debug.Log("Palanca correcta activada.");
            puertas.SetActive(true);  // Activar las puertas y textos
        }
        else
        {
            Debug.Log("Palanca incorrecta, se despierta el Lado Rojo.");
            textoTitulo.text = "�TE DIJE QUE NO ME DESPIERTES!";  // Cambiar texto
            textoError.color = Color.red;  // Cambiar color del texto de error a rojo
            Invoke("MostrarGameOver", 1.5f);  // Mostrar Game Over despu�s de 1.5 segundos
        }
    }

    void MostrarGameOver()
    {
        panelGameOver.SetActive(true);  // Activar el panel de Game Over
    }

    // M�todo para reiniciar el juego
    public void ReiniciarJuego()
    {
        palancaActivada = false;  // Asegurar que se reinicia el estado
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);  // Recargar la escena actual
    }
}
