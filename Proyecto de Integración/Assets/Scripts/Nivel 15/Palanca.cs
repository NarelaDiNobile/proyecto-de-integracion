using UnityEngine;
using TMPro;
using UnityEngine.Rendering.Universal;

public class Palanca : MonoBehaviour
{
    private Animator animator;
    public bool esCorrecta; // Define si esta palanca es la correcta

    public GameObject puertas;  // Contenedor con las dos puertas y textos
    public TMP_Text textoTitulo;  // Texto principal que cambia si fallas
    public TMP_Text textoIncorrecto;  // Texto que se muestra cuando la palanca es incorrecta
    public Light2D linternaLight;  // Luz de la linterna
    public GameObject panelGameOver;  // Panel de Game Over

    // Nuevos textos
    public TMP_Text nuevoTexto1;  // Primer texto adicional
    public TMP_Text nuevoTexto2;  // Segundo texto adicional

    // Referencias a la otra palanca (la que se bloquea)
    public Palanca otraPalanca;  // Referencia a la otra palanca

    private bool activada = false;  // Si la palanca ya fue activada
    private float tiempoRestante = 30f;  // Tiempo en segundos para el contador (15 segundos)
    public TMP_Text contadorTexto;  // Texto del contador

    private bool contadorActivo = true;  // Variable para controlar si el contador debe estar activo
    private bool modoSilencioso = false;  // Variable para el modo silencioso

    void Start()
    {
        animator = GetComponent<Animator>();
        puertas.SetActive(false);  // Las puertas est�n desactivadas al inicio
        panelGameOver.SetActive(false);  // El panel de Game Over est� desactivado

        // Inicializar los textos correctamente
        textoTitulo.gameObject.SetActive(true);  // Asegurarse de que el texto inicial est� activo
        textoIncorrecto.gameObject.SetActive(false);  // Asegurarse de que el texto incorrecto est� desactivado

        // Inicializamos los nuevos textos
        nuevoTexto1.gameObject.SetActive(true);  // Asegurarse de que el primer nuevo texto est� activo
        nuevoTexto2.gameObject.SetActive(true);  // Asegurarse de que el segundo nuevo texto est� activo

        // Desactivar los textos si el modo silencioso est� activado
        if (modoSilencioso)
        {
            textoTitulo.gameObject.SetActive(false);
            textoIncorrecto.gameObject.SetActive(false);
            nuevoTexto1.gameObject.SetActive(false);  // Desactivar primer texto
            nuevoTexto2.gameObject.SetActive(false);  // Desactivar segundo texto
        }

        if (otraPalanca != null)
        {
            otraPalanca.activada = false;  // Aseg�rate de que la otra palanca no est� activada al inicio
            otraPalanca.BloquearPalanca(false);  // Hacer que la otra palanca est� desbloqueada al inicio
        }

        // Mostrar el tiempo en el contador
        contadorTexto.text = tiempoRestante.ToString("F0");
    }

    void Update()
    {
        // Solo actualizar el contador si est� activo
        if (contadorActivo)
        {
            // Actualizar el contador si el panel de Game Over no est� activo
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;  // Restar el tiempo
                contadorTexto.text = Mathf.Clamp(tiempoRestante, 0, 30).ToString("F0");  // Mostrar el tiempo restante (no deber�a ser negativo)
            }
            else
            {
                // Si el tiempo llega a 0, mostrar el Game Over
                MostrarGameOver();
            }
        }
    }

    void OnMouseDown()
    {
        if (activada) return;  // Si ya est� activada, no hace nada

        activada = true;  // Marcar esta palanca como activada
        animator.SetTrigger("Activar");

        // Bloquear la otra palanca si no es correcta
        if (otraPalanca != null)
        {
            otraPalanca.BloquearPalanca(true);  // Bloquear la otra palanca
        }

        if (esCorrecta)
        {
            Debug.Log("Palanca correcta activada.");
            puertas.SetActive(true);  // Activar las puertas y textos
            // Desactivar el texto incorrecto y mantener el correcto
            textoIncorrecto.gameObject.SetActive(false);
            textoTitulo.gameObject.SetActive(true); // Asegurarse de que el texto correcto est� activo

            // Detener el contador si la palanca es correcta
            contadorActivo = false;  // Pausar el contador

            // Si el modo silencioso est� activado, tambi�n desactivar los textos
            if (modoSilencioso)
            {
                textoTitulo.gameObject.SetActive(false);
                textoIncorrecto.gameObject.SetActive(false);
                nuevoTexto1.gameObject.SetActive(false);  // Desactivar primer texto
                nuevoTexto2.gameObject.SetActive(false);  // Desactivar segundo texto
            }
        }
        else
        {
            Debug.Log("Palanca incorrecta, se despierta el Lado Rojo.");
            textoTitulo.gameObject.SetActive(false);  // Desactivar el texto correcto
            textoIncorrecto.gameObject.SetActive(true);  // Activar el texto incorrecto
            linternaLight.color = Color.red;  // Cambiar la luz a rojo
            Invoke("MostrarGameOver", 1.5f);  // Mostrar Game Over despu�s de 1.5 segundos
        }
    }

    // M�todo para bloquear o desbloquear la palanca
    public void BloquearPalanca(bool bloquear)
    {
        if (bloquear)
        {
            // Cambiar apariencia para bloquear la palanca (puedes desactivarla o cambiar su color)
            GetComponent<Collider2D>().enabled = false;  // Desactivar el collider para que no se pueda interactuar con ella
            // Aqu� podr�as cambiar el color de la palanca, poner un texto que diga "Bloqueada", etc.
            Debug.Log("La palanca est� bloqueada.");
        }
        else
        {
            // Volver a activar la palanca
            GetComponent<Collider2D>().enabled = true;  // Reactivar el collider
            // Aqu� puedes cambiar el color de la palanca de vuelta a su estado inicial.
            Debug.Log("La palanca est� desbloqueada.");
        }
    }

    void MostrarGameOver()
    {
        panelGameOver.SetActive(true);  // Activar el panel de Game Over
        contadorTexto.gameObject.SetActive(false);  // Desactivar el texto del contador
    }
}
