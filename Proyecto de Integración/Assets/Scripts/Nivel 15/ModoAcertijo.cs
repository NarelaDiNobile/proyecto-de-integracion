using UnityEngine;
using TMPro;
using System.Collections;

public class ModoAcertijo : MonoBehaviour
{
    public GameObject panelModoAcertijo;  // El panel completo para el modo acertijo
    public TMP_Text textoFondo;  // El texto inicial: "No quiero que me DESPIERTES."
    public TMP_Text textoError;  // El texto de error: "�Te dije que no me despiertes!"
    public GameObject palancaRoja;  // Palanca roja
    public GameObject palancaAzul;  // Palanca azul
    public TMP_Text textoPuertaAzul;  // Texto de la puerta azul
    public TMP_Text textoPuertaRoja;  // Texto de la puerta roja
    public Color[] colores;  // Array de colores para los sprites
    public float tiempoCambioColor = 2f;  // Tiempo entre cada cambio de color en los sprites
    public GameObject panelGameOver;  // Panel de Game Over (si el jugador se equivoca)

    private bool acertijoActivo = false;
    private int colorMasRepetido;

    // Para llevar la cuenta de las veces que aparece cada color
    private int[] conteoColores = new int[2];  // 0=Rojo, 1=Azul

    // Referencias a los GameObjects con los sprites
    public GameObject spriteRojo;  // Sprite rojo
    public GameObject spriteAzul;  // Sprite azul

    void Start()
    {
        panelModoAcertijo.SetActive(false);  // El panel del modo acertijo est� oculto al principio
        panelGameOver.SetActive(false);  // El panel de Game Over tambi�n est� oculto
        textoError.gameObject.SetActive(false);  // Desactivar el texto de error al principio
        textoFondo.text = "No quiero que me DESPIERTES.";  // Texto inicial
    }

    public void ActivarModoAcertijo()
    {
        // Activar todos los elementos cuando se presiona el bot�n NO
        panelModoAcertijo.SetActive(true);
        textoFondo.text = "No quiero que me molestes.";  // Texto del fondo
        textoError.gameObject.SetActive(false);  // Aseg�rate de que el texto de error est� oculto
        StartCoroutine(CambiarColoresSprites());
    }

    // Cambiar los colores en los sprites
    IEnumerator CambiarColoresSprites()
    {
        // Resetear conteo de colores
        conteoColores[0] = 0; // Rojo
        conteoColores[1] = 0; // Azul

        // Duraci�n del ciclo de 10 segundos con colores cambiando
        int tiempoActivacion = 10;

        // Realizar el ciclo y asegurar que el rojo y azul se repitan las veces correctas
        for (int i = 0; i < tiempoActivacion; i++)
        {
            int colorIndex = -1;

            // Asegurar que el rojo se repita 3 veces y el azul 5 veces
            if (conteoColores[0] < 3)  // Rojo
            {
                colorIndex = 0;
            }
            else if (conteoColores[1] < 5)  // Azul
            {
                colorIndex = 1;
            }
            else
            {
                // Si ya se ha alcanzado el m�ximo de repeticiones de ambos colores, selecciona el que falta
                colorIndex = conteoColores[0] < 3 ? 0 : 1;
            }

            // Cambiar el color del sprite correspondiente
            if (colorIndex == 0)  // Rojo
            {
                spriteRojo.GetComponent<SpriteRenderer>().color = colores[0];  // Cambia color del sprite rojo
            }
            else if (colorIndex == 1)  // Azul
            {
                spriteAzul.GetComponent<SpriteRenderer>().color = colores[1];  // Cambia color del sprite azul
            }

            // Incrementar el contador de colores
            conteoColores[colorIndex]++;

            // Esperar el tiempo de cambio de color
            yield return new WaitForSeconds(tiempoCambioColor);
        }

        // Al finalizar, determinar el color m�s repetido
        int maxColor = 0;
        for (int i = 1; i < conteoColores.Length; i++)
        {
            if (conteoColores[i] > conteoColores[maxColor])
                maxColor = i;
        }

        // Color m�s repetido ser� el correcto
        colorMasRepetido = maxColor;
    }

    // M�todo para activar las puertas y textos si la respuesta es correcta
    public void ActivarPuertas(bool esCorrecta)
    {
        if (esCorrecta)
        {
            textoPuertaAzul.gameObject.SetActive(true);
            textoPuertaRoja.gameObject.SetActive(true);
        }
        else
        {
            // Si la respuesta es incorrecta
            textoError.gameObject.SetActive(true);
            StartCoroutine(ActivarGameOver());
        }
    }

    public void ComprobarRespuesta(GameObject palancaSeleccionada)
    {
        // Aqu� asumimos que el color m�s repetido ser� 1 para azul, por ejemplo
        if (palancaSeleccionada == palancaAzul && colorMasRepetido == 1)  // Si la palanca azul es la correcta
        {
            Debug.Log("Respuesta Correcta!");
            textoFondo.text = "�Respuesta correcta!";
            ActivarPuertas(true);  // Activar puertas si la respuesta es correcta
        }
        else
        {
            Debug.Log("Respuesta Incorrecta");
            textoFondo.text = "�TE DIJE QUE NO ME DESPIERTES!";

            // Activar el texto de error
            textoError.gameObject.SetActive(true);
            textoError.color = Color.red;

            // Activar Game Over despu�s de 2 segundos
            StartCoroutine(ActivarGameOver());
        }
    }

    // Activar Game Over despu�s de 2 segundos
    IEnumerator ActivarGameOver()
    {
        yield return new WaitForSeconds(5f);
        panelGameOver.SetActive(true);
    }
}
