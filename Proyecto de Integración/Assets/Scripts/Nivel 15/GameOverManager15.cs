using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager15 : MonoBehaviour
{
    public GameObject panelGameOver;  // Panel de Game Over
    public GameObject modoSilenciado;  // Referencia al modo silenciado

    void Start()
    {
        panelGameOver.SetActive(false);  // Asegurar que est� desactivado al inicio

        // Suscribir el evento para cuando se cargue la escena
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Reactivar el Modo Silenciado al cargar la escena
        modoSilenciado.SetActive(true);
    }

    public void MostrarGameOver()
    {
        panelGameOver.SetActive(true);  // Mostrar el panel de Game Over
    }

    public void ReiniciarJuego()
    {
        // Recargar la escena actual
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void IrAlMenu()
    {
        SceneManager.LoadScene("Menu");  // Cambia por el nombre real de tu escena del men�
    }

    public void SalirDelJuego()
    {
        Application.Quit();
        Debug.Log("Saliendo del juego...");
    }

    // Asegurarse de eliminar la suscripci�n para evitar problemas
    private void OnDestroy()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
