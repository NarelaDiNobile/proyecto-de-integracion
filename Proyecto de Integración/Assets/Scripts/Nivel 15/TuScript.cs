using UnityEngine;
using UnityEngine.UI;

public class TuScript : MonoBehaviour
{
    // Referencias al slider y al texto del micr�fono
    public Slider slider;
    public GameObject textoMicr�fono; // Aseg�rate de que el texto est� en un GameObject

    // M�todo para manejar el bot�n NO
    public void BotonNO()
    {
        // Desactivar el slider y el texto del micr�fono
        slider.gameObject.SetActive(false);  // Desactiva el slider
        textoMicr�fono.SetActive(false);     // Desactiva el texto del micr�fono
    }
}
