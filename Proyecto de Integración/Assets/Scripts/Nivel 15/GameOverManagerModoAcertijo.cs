using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManagerModoAcertijo : MonoBehaviour
{
    public GameObject panelGameOver;  // Panel de Game Over

    void Start()
    {
        panelGameOver.SetActive(false);  // Asegurar que est� desactivado al inicio
    }

    public void MostrarGameOver()
    {
        panelGameOver.SetActive(true);
    }

    public void ReiniciarNivel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void IrAlMenu()
    {
        SceneManager.LoadScene("Menu");  // Cambia por el nombre real de tu escena del men�
    }

    public void SalirDelJuego()
    {
        Application.Quit();
        Debug.Log("Saliendo del juego...");  // Para ver en Unity Editor, ya que Application.Quit no funciona en el editor
    }
}
