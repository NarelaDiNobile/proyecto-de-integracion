using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;

public class LaserReflect : MonoBehaviour
{
    public LineRenderer Linea;
    public Transform tr;
    public LayerMask espejoLayer;
    public LayerMask receptorLayer;
    public int maxRebotes = 10;
    public float largoDelLaser = 15f;

    public GameObject textoPuertaRoja;
    public GameObject textoPuertaAzul;
    public GameObject textoExito;
    public GameObject textoTiempoAgotado;
    public GameObject textoSistemaBien;
    public TimerController timerController;
    public GameObject panelGameOver; // Panel de Game Over

    public bool haTocadoReceptor = true;
    public bool puertaRojaTocada = true;
    public bool puertaAzulTocada = true;

    public GameObject botonReiniciar;  // Bot�n de reiniciar
    public TMP_Text textoReiniciar;    // Texto de reiniciar

    public GameObject redDoor, blueDoor;
    public TMP_Text redDoorText, blueDoorText;

    public bool redDoorTouched = true;
    public bool blueDoorTouched = true;

    public TMP_Text mensajePuertaRoja;
    public TMP_Text mensajePuertaAzul;

    public bool puertaRojaDesbloqueada = true;
    public bool puertaAzulDesbloqueada = true;

    public bool puertaRojaTocada7 = true;
    public bool puertaAzulTocada7 = true;

    public static NivelController instance;
    [SerializeField] Animator transitionAnim;

    private void Start()
    {
        if (Linea == null) Linea = GetComponent<LineRenderer>();
        if (tr == null) Debug.LogError("tr (Transform) no est� asignado.");

        if (textoPuertaRoja) textoPuertaRoja.SetActive(false);
        if (textoPuertaAzul) textoPuertaAzul.SetActive(false);
        if (textoExito) textoExito.SetActive(false);
        if (textoSistemaBien) textoSistemaBien.SetActive(false);
        if (panelGameOver) panelGameOver.SetActive(false);
    }

    private void Update()
    {
        DibujarLaser();
        // Si el clic del mouse est� sobre la puerta roja
        if (Input.GetMouseButtonDown(0)) // Bot�n izquierdo del mouse
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.gameObject == redDoor && puertaRojaDesbloqueada)
                {
                    ColisionarConPuerta5("Roja");
                }
                else if (hit.collider.gameObject == blueDoor && puertaAzulDesbloqueada)
                {
                    ColisionarConPuerta5("Azul");
                }
            }
        }
    }

    private void DibujarLaser()
    {
        Vector2 posicionActual = tr.position;
        Vector2 direccion = tr.up;

        Linea.positionCount = 1;
        Linea.SetPosition(0, posicionActual);

        for (int i = 0; i < maxRebotes; i++)
        {
            RaycastHit2D hit = Physics2D.Raycast(posicionActual, direccion, largoDelLaser, espejoLayer | receptorLayer);

            if (hit.collider != null)
            {
                posicionActual = hit.point;
                direccion = Vector2.Reflect(direccion, hit.normal).normalized;

                Linea.positionCount++;
                Linea.SetPosition(i + 1, posicionActual);

                if (((1 << hit.collider.gameObject.layer) & receptorLayer) != 0 && !haTocadoReceptor)
                {
                    haTocadoReceptor = true;
                    ActivarReceptor();
                    break;
                }
            }
            else
            {
                Linea.positionCount++;
                Linea.SetPosition(i + 1, posicionActual + direccion * largoDelLaser);
                break;
            }
        }
    }

    private void ActivarReceptor()
    {
        Debug.Log("�L�ser ha alcanzado el receptor!");
        if (textoPuertaRoja) textoPuertaRoja.SetActive(true);
        if (textoPuertaAzul) textoPuertaAzul.SetActive(true);
        if (textoExito) textoExito.SetActive(true);

        // Desactivar "Tiempo agotado" y activar "El sistema no se ha sobrecargado, bien hecho"
        if (textoTiempoAgotado) textoTiempoAgotado.SetActive(false);
        if (textoSistemaBien) textoSistemaBien.SetActive(true);

        // Desactivar el bot�n de reiniciar y su texto
        if (botonReiniciar)
        {
            botonReiniciar.SetActive(false);
            Debug.Log("Bot�n Reiniciar desactivado.");
        }
        if (textoReiniciar)
        {
            textoReiniciar.gameObject.SetActive(false);
            Debug.Log("Texto Reiniciar desactivado.");
        }


        // Detener el tiempo
        if (timerController != null)
        {
            timerController.DetenerTiempo();
        }
    }

    public void ActivarGameOver()
    {
        if (panelGameOver)
        {
            panelGameOver.SetActive(true);
            Debug.Log("�Game Over activado!");
        }
    }

    public void ColisionarConPuerta5(string puerta)
    {
        Debug.Log("Se toc� la puerta: " + puerta);  // Aseg�rate de que se detecta el clic

        if (puerta == "Roja" && puertaRojaDesbloqueada)
        {
            puertaRojaTocada = true;
            mensajePuertaRoja.text = "�Puerta Roja tocada!";
        }
        else if (puerta == "Azul" && puertaAzulDesbloqueada)
        {
            puertaAzulTocada = true;
            mensajePuertaAzul.text = "�Puerta Azul tocada!";
        }

        // Comprobar si ambas puertas han sido tocadas
        ComprobarYAvanzar();
    }


    public void ComprobarYAvanzar()
    {
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        if (puertaRojaTocada && puertaAzulTocada)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}
