using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;

public class TimerController : MonoBehaviour
{
    public float tiempoRestante = 60f;
    public TMP_Text textoTiempo;
    public GameObject panelAlarma;
    public GameObject botonReiniciar;
    public TMP_Text textoReiniciar;
    public GameObject[] vidas;
    public GameObject panelGameOver; // Panel de Game Over
    public GameObject botonReiniciarNivel; // Bot�n de reiniciar nivel
    public GameObject botonMenu; // Bot�n de men�
    public GameObject botonSalir; // Bot�n de salir
    private int vidasRestantes;
    private bool alarmaActiva = false;
    private bool tiempoAgotado = false;
    private SpriteRenderer spriteAlarma;

    private bool nivelCompletado = false; // Bandera para verificar si el nivel ya fue completado

    void Start()
    {
        spriteAlarma = panelAlarma.GetComponent<SpriteRenderer>();
        botonReiniciar.SetActive(false);  // Desactivamos el bot�n de reiniciar desde el inicio
        textoReiniciar.gameObject.SetActive(false); // Desactivamos el texto de reiniciar desde el inicio
        if (panelGameOver) panelGameOver.SetActive(false); // Panel de Game Over desactivado

        vidasRestantes = PlayerPrefs.GetInt("vidas", vidas.Length);
        ActualizarVidasVisuales();
    }

    void Update()
    {
        if (!nivelCompletado) // Solo cuenta el tiempo si el nivel no ha sido completado
        {
            if (tiempoRestante > 0)
            {
                tiempoRestante -= Time.deltaTime;
                textoTiempo.text = "" + Mathf.Ceil(tiempoRestante) + "";

                if (tiempoRestante <= 10 && !alarmaActiva)
                {
                    alarmaActiva = true;
                    StartCoroutine(ParpadeoAlarma());
                }
            }
            else if (!tiempoAgotado)
            {
                tiempoAgotado = true;
                tiempoRestante = 0;
                textoTiempo.text = "�Tiempo agotado!";

                // Aqu� activamos el bot�n de reiniciar solo si el nivel no est� completado
                if (botonReiniciar && textoReiniciar)
                {
                    botonReiniciar.SetActive(true);
                    textoReiniciar.gameObject.SetActive(true);
                }
            }
        }
    }

    void RestarVida()
    {
        if (vidasRestantes > 0)
        {
            vidasRestantes--;
            PlayerPrefs.SetInt("vidas", vidasRestantes);
            PlayerPrefs.Save();
            ActualizarVidasVisuales();

            if (vidasRestantes > 0)
            {
                botonReiniciar.SetActive(true);  // Activa el bot�n solo cuando queda vida
                textoReiniciar.gameObject.SetActive(true);  // Activa el texto de reiniciar
            }
            else
            {
                ActivarGameOver();
            }
        }

        StopCoroutine(ParpadeoAlarma());
        spriteAlarma.color = new Color(1f, 1f, 1f, 0f);
    }

    public void ReiniciarNivel()
    {
        if (vidasRestantes <= 1) // Si el jugador ten�a 1 vida y reinicia, activar Game Over
        {
            ActivarGameOver();
        }
        else
        {
            vidasRestantes--; // Resta una vida SOLO cuando se presiona "Reiniciar"
            PlayerPrefs.SetInt("vidas", vidasRestantes);
            PlayerPrefs.Save();

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            tiempoRestante = 60f;
            botonReiniciar.SetActive(false);
            textoReiniciar.gameObject.SetActive(false);
            nivelCompletado = false;
        }
    }

    void ActualizarVidasVisuales()
    {
        for (int i = 0; i < vidas.Length; i++)
        {
            vidas[i].SetActive(i < vidasRestantes);
        }
    }

    IEnumerator ParpadeoAlarma()
    {
        while (tiempoRestante > 0)
        {
            spriteAlarma.color = new Color(1f, 0f, 0f, 0.6f);
            yield return new WaitForSeconds(0.5f);
            spriteAlarma.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void DetenerTiempo()
    {
        tiempoRestante = 0;
        textoTiempo.text = "";
        StopAllCoroutines();
    }

    private void ActivarGameOver()
    {
        if (panelGameOver)
        {
            panelGameOver.SetActive(true);
            textoTiempo.text = "�Game Over!";
            Debug.Log("�Game Over activado!");

            // Activar botones de Game Over
            botonReiniciarNivel.SetActive(true);
            botonMenu.SetActive(true);
            botonSalir.SetActive(true);
        }
    }
    public void RestartLevel()
    {
        // Restablecer las vidas a 4 antes de recargar la escena
        PlayerPrefs.SetInt("vidas", 4);
        PlayerPrefs.Save();

        // Mostrar el panel de Game Over antes de recargar la escena
        if (panelGameOver)
        {
            panelGameOver.SetActive(true);
        }

        // Hacer una peque�a pausa antes de recargar la escena para que el panel de Game Over sea visible
        StartCoroutine(ReiniciarNivelConRetraso());
    }

    private IEnumerator ReiniciarNivelConRetraso()
    {
        // Esperar un par de segundos (puedes ajustar el tiempo a tu preferencia)
        yield return new WaitForSeconds(1f);

        // Ahora recargamos la escena despu�s de la pausa
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void VolverAlMenu()
    {
        // Carga la escena del men� principal
        SceneManager.LoadScene("Menu"); // Aseg�rate de que tu escena se llama "Menu"
    }

    public void SalirDelJuego()
    {
        // Cierra la aplicaci�n
        Application.Quit();
    }

    // M�todo para marcar que el nivel ha sido completado
    public void NivelCompletado()
    {
        nivelCompletado = true;
        botonReiniciar.SetActive(false);  // Desactiva el bot�n de reiniciar cuando el nivel est� completado
        textoReiniciar.gameObject.SetActive(false); // Desactiva el texto de reiniciar cuando el nivel est� completado
        // Aqu� puedes poner la l�gica para mostrar el mensaje de �xito o cualquier otra acci�n
    }
}
