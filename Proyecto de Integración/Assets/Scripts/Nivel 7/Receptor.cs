using UnityEngine;

public class Receptor : MonoBehaviour
{
    // Puedes agregar acciones aqu� cuando el l�ser toque el receptor
    public void Activar()
    {
        Debug.Log("�Receptor activado!");
        // Aqu� puedes agregar el c�digo que quieras que pase cuando el l�ser toque el receptor
    }
}
