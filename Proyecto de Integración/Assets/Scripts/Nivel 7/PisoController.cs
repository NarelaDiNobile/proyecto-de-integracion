using UnityEngine;

public class PisoController : MonoBehaviour
{
    public Color colorSeleccionado = new Color(0.1f, 0.1f, 0.1f); // Color de selecci�n
    private Color colorOriginal; // Color original del piso
    private static PisoController pisoSeleccionado = null; // Piso seleccionado actualmente

    private void Start()
    {
        // Guardamos el color original del piso
        colorOriginal = GetComponent<SpriteRenderer>().color;
    }

    private void OnMouseDown()
    {
        // Si ya hay un piso seleccionado, intercambiamos posiciones
        if (pisoSeleccionado != null && pisoSeleccionado != this)
        {
            IntercambiarPisoss(pisoSeleccionado);
            pisoSeleccionado.Deseleccionar();
        }
        else
        {
            Seleccionar();
        }
    }

    // Funci�n para seleccionar el piso
    public void Seleccionar()
    {
        GetComponent<SpriteRenderer>().color = colorSeleccionado;
        pisoSeleccionado = this; // Marcar este piso como seleccionado
    }

    // Funci�n para deseleccionar el piso
    public void Deseleccionar()
    {
        GetComponent<SpriteRenderer>().color = colorOriginal;
        pisoSeleccionado = null;
    }

    // Funci�n para intercambiar la posici�n entre dos pisos
    private void IntercambiarPisoss(PisoController otroPiso)
    {
        Vector3 tempPos = transform.position;
        transform.position = otroPiso.transform.position;
        otroPiso.transform.position = tempPos;
    }
}
