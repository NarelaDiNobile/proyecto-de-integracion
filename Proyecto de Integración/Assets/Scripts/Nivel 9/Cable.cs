using UnityEngine;

public class Cable : MonoBehaviour
{
    public string colorCable; // Color del cable
    private bool cortado = false;

    void OnMouseDown()
    {
        if (!cortado)
        {
            cortado = true;
            gameObject.SetActive(false); // Desactiva el cable visualmente

            // Enviar el color del cable al GameManager
            GameManager9.instance.VerificarCable(colorCable);
        }
    }
}
