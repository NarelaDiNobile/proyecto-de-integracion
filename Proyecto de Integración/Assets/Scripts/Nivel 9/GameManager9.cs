using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManager9 : MonoBehaviour
{
    public static GameManager9 instance;

    private int cablesCorrectosCortados = 0;
    private List<string> cablesCorrectos = new List<string> { "Verde", "Celeste", "Rojo" };

    public TMP_Text temporizadorTMP; // Contador de tiempo
    public TMP_Text estadoBombaTMP; // Estado de la bomba
    public TMP_Text bienHechoTMP; // Texto "Bien Hecho"

    public GameObject explosionEffect; // Efecto de explosi�n
    public GameObject gameOverPanel; // Panel de Game Over
    public GameObject pistasPanel; // Panel con las pistas
    public GameObject puertaRoja; // Objeto puerta roja
    public GameObject puertaAzul; // Objeto puerta azul
    public GameObject textoPuertaRoja; // Texto "Tocar Puerta Roja"
    public GameObject textoPuertaAzul; // Texto "Tocar Puerta Azul"

    public AudioSource explosionSound; // Sonido de explosi�n

    // Vidas
    public GameObject vida1; // Imagen de la primera vida
    public GameObject vida2; // Imagen de la segunda vida
    private int vidas = 2; // N�mero de vidas

    private float tiempoRestante = 300f; // 5 minutos
    private bool bombaActiva = true;

    public GameObject reiniciarBoton;
    public GameObject menuBoton;
    public GameObject salirBoton;


    void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        estadoBombaTMP.text = "Bomba Activa";
        bienHechoTMP.gameObject.SetActive(false); // Ocultar "Bien Hecho"

        // Asegurar que las puertas y textos est�n desactivados al iniciar el nivel
        puertaRoja.SetActive(false);
        puertaAzul.SetActive(false);
        textoPuertaRoja.SetActive(false);
        textoPuertaAzul.SetActive(false);

        ActualizarTemporizador();

        // Asegurar que las vidas est�n activas al inicio
        vida1.SetActive(true);
        vida2.SetActive(true);
    }


    void Update()
    {
        if (bombaActiva)
        {
            tiempoRestante -= Time.deltaTime;
            ActualizarTemporizador();

            if (tiempoRestante <= 0)
            {
                ExplotaBomba();
            }
        }
    }

    void ActualizarTemporizador()
    {
        int minutos = Mathf.FloorToInt(tiempoRestante / 60);
        int segundos = Mathf.FloorToInt(tiempoRestante % 60);
        temporizadorTMP.text = string.Format("{0:00}:{1:00}", minutos, segundos);
    }

    public void VerificarCable(string colorCable)
    {
        if (!bombaActiva) return;

        if (cablesCorrectos.Contains(colorCable))
        {
            cablesCorrectosCortados++;

            if (cablesCorrectosCortados == cablesCorrectos.Count)
            {
                DesactivarBomba();
            }
        }
        else
        {
            PerderVida(); // Si es incorrecto, perder una vida
            RestarTiempo(60); // Restar 1 minuto
        }
    }

    void PerderVida()
    {
        if (vidas == 2)
        {
            // Restar la primera vida
            vida1.SetActive(false);
            vidas--;
        }
        else if (vidas == 1)
        {
            // Restar la segunda vida
            vida2.SetActive(false);
            vidas--;
            // Si pierde todas las vidas, Game Over
            GameOver();
        }
    }

    void RestarTiempo(float tiempo)
    {
        tiempoRestante -= tiempo; // Restar el tiempo especificado (en este caso, 60 segundos)
        if (tiempoRestante <= 0)
        {
            ExplotaBomba(); // Si el tiempo llega a 0, la bomba explota
        }
        else
        {
            ActualizarTemporizador(); // Actualizar la visualizaci�n del tiempo restante
        }
    }

    void DesactivarBomba()
    {
        bombaActiva = false;
        estadoBombaTMP.text = "Bomba Desactivada";
        Debug.Log("�Bomba Desactivada!");

        pistasPanel.SetActive(false); // Ocultar pistas
        bienHechoTMP.gameObject.SetActive(true); // Mostrar "Bien Hecho"

        // Activar puertas y textos
        puertaRoja.SetActive(true);
        puertaAzul.SetActive(true);
        textoPuertaRoja.SetActive(true);
        textoPuertaAzul.SetActive(true);
    }

    void ExplotaBomba()
    {
        bombaActiva = false; // Pausar el temporizador
        tiempoRestante = 0; // Asegurar que el contador quede en 0
        ActualizarTemporizador(); // Mostrar 00:00 en pantalla

        estadoBombaTMP.text = "�BOOM! La bomba explot�.";
        Debug.Log("�BOOM! La bomba explot�.");

        explosionEffect.SetActive(true); // Activar efecto de explosi�n
        explosionSound.Play(); // Reproducir sonido de explosi�n

        // Esperar 3 segundos antes de mostrar el Game Over
        Invoke("MostrarGameOver", 2f);
    }


    void MostrarGameOver()
    {
        // Desactivar las puertas y sus textos
        puertaRoja.SetActive(false);
        puertaAzul.SetActive(false);
        textoPuertaRoja.SetActive(false);
        textoPuertaAzul.SetActive(false);

        // Mostrar el panel de Game Over
        gameOverPanel.SetActive(true);
        Debug.Log("Game Over");

        // Activar los botones
        reiniciarBoton.SetActive(true);
        menuBoton.SetActive(true);
        salirBoton.SetActive(true);
    }



    void GameOver()
    {
        bombaActiva = false; // Pausar el temporizador
        ExplotaBomba(); // Activar la explosi�n antes del Game Over
    }


    public void ReiniciarNivel()
    {
        // Asegurar que las puertas est�n desactivadas antes de recargar la escena
        puertaRoja.SetActive(false);
        puertaAzul.SetActive(false);
        textoPuertaRoja.SetActive(false);
        textoPuertaAzul.SetActive(false);

        // Resetear variables importantes
        vidas = 2;
        cablesCorrectosCortados = 0;
        tiempoRestante = 300f; // Reiniciar el tiempo
        bombaActiva = true;

        // Recargar la escena actual para reiniciar el nivel
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }



    public void IrAlMenu()
    {
        // Aqu� debes cargar la escena del men� (aseg�rate de tener el nombre correcto de la escena de men�)
        SceneManager.LoadScene("Menu"); // Aseg�rate de que "Menu" sea el nombre de tu escena de men�
    }

    public void SalirDelJuego()
    {
        // Salir del juego (esto solo funciona en una compilaci�n final, no en el editor)
        Application.Quit();
    }
}

