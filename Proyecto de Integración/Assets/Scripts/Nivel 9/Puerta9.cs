using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Puerta9 : MonoBehaviour
{
    public GameObject redDoor; // La puerta roja
    public GameObject blueDoor; // La puerta azul
    private bool redDoorTouched = false; // Indica si la puerta roja fue tocada
    private bool blueDoorTouched = false; // Indica si la puerta azul fue tocada

    public static Puerta9 instance;
    [SerializeField] Animator transitionAnim;
    void Start()
    {
        // Aseg�rate de que las puertas est�n inicialmente invisibles
        redDoor.SetActive(false); // Desactivar las puertas al iniciar
        blueDoor.SetActive(false);
    }


    void Update()
    {
        // Detectar clic del mouse en la puerta roja
        if (Input.GetMouseButtonDown(0)) // Clic izquierdo
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0; // Aseguramos que la posici�n z sea 0

            // Verificar si el clic fue sobre la puerta roja
            if (redDoor.GetComponent<Collider2D>().OverlapPoint(mousePos))
            {
                redDoorTouched = true;
            }

            // Verificar si el clic fue sobre la puerta azul
            if (blueDoor.GetComponent<Collider2D>().OverlapPoint(mousePos))
            {
                blueDoorTouched = true;
            }

            StartCoroutine(LoadLevel());
        }
    }

    IEnumerator LoadLevel()
    {
        if (redDoorTouched && blueDoorTouched)
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }


}
