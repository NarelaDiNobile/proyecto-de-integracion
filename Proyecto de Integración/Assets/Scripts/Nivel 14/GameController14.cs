using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController14 : MonoBehaviour
{
    public GameObject[] combinaciones;
    public TMP_Text scoreText;
    public TMP_Text timerText;
    public GameObject[] vidas;
    public GameObject gameOverPanel;
    public Button reiniciarButton;
    public Button menuButton;
    public Button salirButton;

    public GameObject puertaRoja;
    public GameObject puertaAzul;
    public TMP_Text textoRojo;
    public TMP_Text textoAzul;

    private int score = 0;
    private float timer;
    private int currentCombinationIndex = 0;
    private bool correctAnswer;
    private int vidasRestantes = 4;
    private bool puertaRojaClickeada = false;
    private bool puertaAzulClickeada = false;
    private bool juegoPausado = false; 

    public GameObject bienHechoTexto;

    void Start()
    {
        scoreText.text = "" + score.ToString();
        ActivateCombination(0);
        timer = GetTimerForCombination(currentCombinationIndex);
        StartCoroutine(TimerCountdown());

        gameOverPanel.SetActive(false);
        textoRojo.gameObject.SetActive(false);
        textoAzul.gameObject.SetActive(false);
        puertaRoja.SetActive(false);
        puertaAzul.SetActive(false);

        reiniciarButton.onClick.AddListener(ReiniciarJuego);
        menuButton.onClick.AddListener(IrAlMenu);
        salirButton.onClick.AddListener(SalirDelJuego);
    }

    void ActivateCombination(int index)
    {
        for (int i = 0; i < combinaciones.Length; i++)
        {
            combinaciones[i].SetActive(i == index);
        }
    }

    public void HandleAnswer(bool isCorrect)
    {
        if (juegoPausado) return; 

        correctAnswer = isCorrect;
        combinaciones[currentCombinationIndex].SetActive(false);

        if (correctAnswer)
        {
            score -= 15;
        }
        else
        {
            score += 15;
        }

        scoreText.text = score.ToString();

        if (score >= 150)
        {
            bienHechoTexto.SetActive(true);
        }

        ActivateNextCombination();
    }

    void ActivateNextCombination()
    {
        currentCombinationIndex++;

        if (currentCombinationIndex < combinaciones.Length)
        {
            StopAllCoroutines();
            timer = GetTimerForCombination(currentCombinationIndex);
            timerText.text = timer.ToString("F0");
            ActivateCombination(currentCombinationIndex);
            StartCoroutine(TimerCountdown());
        }
        else
        {
            EvaluarResultadoFinal();
        }
    }

    float GetTimerForCombination(int index)
    {
        if (index < 3) return 20f;
        if (index < 6) return 15f;
        if (index < 9) return 10f;
        if (index < 12) return 5f;
        return 3f;
    }

    IEnumerator TimerCountdown()
    {
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            timerText.text = timer.ToString("F0");
            yield return null;
        }

        PerderVida();
    }

    void PerderVida()
    {
        if (score >= 150) return; 

        if (vidasRestantes > 0)
        {
            vidasRestantes--;
            vidas[vidasRestantes].SetActive(false);
            ActivateNextCombination();
        }

        if (vidasRestantes <= 0)
        {
            GameOver();
        }
    }


    void EvaluarResultadoFinal()
    {
        juegoPausado = true; 

        if (score >= 150)
        {
            textoRojo.gameObject.SetActive(true);
            textoAzul.gameObject.SetActive(true);
            puertaRoja.SetActive(true);
            puertaAzul.SetActive(true);
        }
        else
        {
            GameOver();
        }
    }

    public void ClickEnPuertaRoja()
    {
        if (!juegoPausado) return;
        puertaRojaClickeada = true;
        VerificarPuertas();
    }

    public void ClickEnPuertaAzul()
    {
        if (!juegoPausado) return;
        puertaAzulClickeada = true;
        VerificarPuertas();
    }

    void VerificarPuertas()
    {
        if (puertaRojaClickeada && puertaAzulClickeada)
        {
            SceneManager.LoadScene("Nivel 15");
        }
    }

    void GameOver()
    {
        juegoPausado = true;
        gameOverPanel.SetActive(true);
    }

    void ReiniciarJuego()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void IrAlMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    void SalirDelJuego()
    {
        Application.Quit();
    }
}
