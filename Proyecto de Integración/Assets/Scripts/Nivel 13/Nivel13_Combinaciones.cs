using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;

public class Nivel13_Combinaciones : MonoBehaviour
{
    [System.Serializable]
    public class Combinacion
    {
        public string textoCombinacion;
        public string pista;
        public string respuestaCorrecta;
    }

    public Combinacion[] combinaciones;
    private int indiceActual = 0;
    private int vidas = 4;

    [Header("Referencias UI")]
    public TMP_Text textoCombinacion;
    public TMP_InputField inputRespuesta;
    public Button botonPista;
    public TMP_Text textoPista;
    public GameObject panelGameOver;
    public GameObject[] vidasImagenes;

    // Referencias para los textos de la puerta
    public TMP_Text textoPuertaRoja;
    public TMP_Text textoPuertaAzul;

    // Referencias a las puertas (como GameObject)
    public GameObject puertaRoja;
    public GameObject puertaAzul;

    private bool isVerificando = false; // Controla si se est� verificando la respuesta
    private bool puertaRojaTocada = false;
    private bool puertaAzulTocada = false;

    void Start()
    {
        IniciarCombinacion();

        // Inicializa los textos de las puertas como desactivados
        textoPuertaRoja.gameObject.SetActive(false);
        textoPuertaAzul.gameObject.SetActive(false);

        // Desactivar la verificaci�n cuando el input pierde foco
        inputRespuesta.onEndEdit.AddListener(VerificarAlEnter);
    }

    void IniciarCombinacion()
    {
        if (indiceActual >= combinaciones.Length)
        {
            Debug.Log("Nivel completado!");
            return;
        }

        textoCombinacion.text = combinaciones[indiceActual].textoCombinacion;
        textoPista.text = combinaciones[indiceActual].pista;
        textoPista.gameObject.SetActive(false); // Inicia con la pista oculta
        inputRespuesta.text = "";
        inputRespuesta.interactable = true;
        inputRespuesta.GetComponent<Image>().color = Color.white;
        botonPista.interactable = true;
    }

    private void VerificarAlEnter(string text)
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            VerificarRespuesta();
        }
    }

    public void VerificarRespuesta()
    {
        if (isVerificando) return;

        isVerificando = true;
        string respuestaJugador = inputRespuesta.text.Trim().ToLower();
        string respuestaCorrecta = combinaciones[indiceActual].respuestaCorrecta.ToLower();

        if (string.IsNullOrEmpty(respuestaJugador))
        {
            Debug.Log("Por favor, ingrese una respuesta.");
            isVerificando = false;
            return;
        }

        if (respuestaJugador == respuestaCorrecta)
        {
            inputRespuesta.GetComponent<Image>().color = Color.green;
            inputRespuesta.interactable = false;
            botonPista.interactable = false;
            textoPista.gameObject.SetActive(false);
            StartCoroutine(SiguienteCombinacionConRetraso());
        }
        else
        {
            inputRespuesta.GetComponent<Image>().color = Color.red;
            RestarVida();
            inputRespuesta.interactable = true;
        }

        isVerificando = false;
    }

    public void MostrarPista()
    {
        textoPista.gameObject.SetActive(!textoPista.gameObject.activeSelf);
    }

    void RestarVida()
    {
        if (vidas > 0)
        {
            vidas--;
            if (vidas >= 0 && vidas < vidasImagenes.Length)
            {
                vidasImagenes[vidas].SetActive(false);
            }

            if (vidas <= 0)
            {
                panelGameOver.SetActive(true);
            }
        }
    }

    IEnumerator SiguienteCombinacionConRetraso()
    {
        yield return new WaitForSeconds(1f);
        indiceActual++;

        if (indiceActual < combinaciones.Length)
        {
            IniciarCombinacion();
        }
        else
        {
            textoPuertaRoja.gameObject.SetActive(true);
            textoPuertaAzul.gameObject.SetActive(true);
            Debug.Log("�Nivel completado!");
        }
    }

    public void IrAlMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void ReiniciarNivel()
    {
        vidas = 4;
        foreach (GameObject vida in vidasImagenes) vida.SetActive(true);
        panelGameOver.SetActive(false);
        indiceActual = 0;
        IniciarCombinacion();
    }

    public void SalirJuego()
    {
        Application.Quit();
    }

    // M�todo para detectar cuando se hace clic en la puerta roja
    private void OnMouseDownPuertaRoja()
    {
        if (!puertaRojaTocada)
        {
            puertaRojaTocada = true;
            VerificarTransicionNivel();
        }
    }

    // M�todo para detectar cuando se hace clic en la puerta azul
    private void OnMouseDownPuertaAzul()
    {
        if (!puertaAzulTocada)
        {
            puertaAzulTocada = true;
            VerificarTransicionNivel();
        }
    }

    void OnMouseDown()
    {
        if (gameObject.name == "Roja") // Cambia el nombre de acuerdo a tu objeto
        {
            OnMouseDownPuertaRoja();
        }
        else if (gameObject.name == "Azul")
        {
            OnMouseDownPuertaAzul();
        }
    }


    // Verifica si ambas puertas han sido tocadas y cambia la escena
    private void VerificarTransicionNivel()
    {
        if (puertaRojaTocada && puertaAzulTocada)
        {
            SceneManager.LoadScene("Nivel 14");
        }
    }
}
