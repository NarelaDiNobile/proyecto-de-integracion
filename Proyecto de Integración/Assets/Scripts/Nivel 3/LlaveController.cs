using UnityEngine;

public class LlaveController : MonoBehaviour
{
    public GameObject[] lifeSprites;  // Array de sprites de vidas
    private int livesRemaining;
    private Vector3 startPosition;  // Posici�n inicial de la llave
    private bool isDragging = false; // Indica si la llave est� siendo arrastrada
    private Vector3 offset; // Para el offset entre la llave y el mouse
    private Camera mainCamera;  // C�mara principal para obtener la posici�n del mouse
    public GameObject panelGameOver;  // Panel de Game Over

    void Start()
    {
        startPosition = transform.position;  // Guardar la posici�n inicial de la llave
        livesRemaining = lifeSprites.Length;  // Asignar el n�mero de vidas
        mainCamera = Camera.main; // Obtener la c�mara principal
        panelGameOver.SetActive(false); // Asegurarse de que el panel de Game Over est� desactivado al principio
    }

    void Update()
    {
        // Si la llave est� siendo arrastrada, moverla con el mouse
        if (isDragging)
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector3 targetPosition = new Vector3(mousePosition.x + offset.x, mousePosition.y + offset.y, 0);
            transform.position = targetPosition;
        }
    }

    void OnMouseDown()
    {
        // Iniciar el arrastre de la llave
        Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        offset = transform.position - mousePosition;
        isDragging = true;
    }

    void OnMouseUp()
    {
        // Detener el arrastre cuando el jugador suelte el bot�n del mouse
        isDragging = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Pared"))  // Detecta solo objetos con la etiqueta "Pared"
        {
            LoseLife();
            ResetPosition();
        }
    }

    private void LoseLife()
    {
        if (livesRemaining > 0)
        {
            livesRemaining--; // Restar una vida
            lifeSprites[livesRemaining].SetActive(false);  // Desactivar sprite de vida

            if (livesRemaining <= 0)
            {
                // Game Over: Activar el panel de Game Over y pausar el juego
                ActivarGameOver();
            }
        }
    }

    private void ResetPosition()
    {
        // Reiniciar la llave a la posici�n inicial sin arrastrarla
        transform.position = startPosition;  // Reestablecer la posici�n de la llave
        isDragging = false;  // Detener el arrastre
    }

    private void ActivarGameOver()
    {
        // Mostrar el panel de Game Over
        panelGameOver.SetActive(true);
        Time.timeScale = 0; // Pausar el juego
    }
}
