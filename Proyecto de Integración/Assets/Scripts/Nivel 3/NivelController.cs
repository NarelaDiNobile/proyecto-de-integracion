using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NivelController : MonoBehaviour
{
    public GameObject panelGameOver;   // Panel de Game Over
    public PuertaController puertaRoja;
    public PuertaController puertaAzul;
    public GameObject[] lifeSprites;   // Sprites de vidas

    private int vidasRestantes;

    public static NivelController instance;
    [SerializeField] Animator transitionAnim;
    void Start()
    {
        vidasRestantes = lifeSprites.Length;
        panelGameOver.SetActive(false);  // Inicialmente el panel est� desactivado
    }

    // M�todo para restar una vida
    public void PerderVida()
    {
        if (vidasRestantes > 0)
        {
            vidasRestantes--;  // Restar una vida
            lifeSprites[vidasRestantes].SetActive(false);  // Desactivar sprite de vida

            // Si se acaban las vidas, activar Game Over
            if (vidasRestantes <= 0)
            {
                ActivarGameOver();
            }
        }
    }

    // Activar el panel de Game Over
    public void ActivarGameOver()
    {
        panelGameOver.SetActive(true);  // Mostrar el panel de Game Over
        Time.timeScale = 0;  // Pausar el juego
    }

    // M�todo para reiniciar el nivel (puede ser llamado desde el bot�n del panel Game Over)
    public void ReiniciarNivel()
    {
        Time.timeScale = 1;  // Reanudar el juego
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);  // Volver a cargar la escena actual
    }

    // M�todo para ir al men� principal (puede ser llamado desde el bot�n del panel Game Over)
    public void VolverMenu()
    {
        Time.timeScale = 1;  // Reanudar el juego
        SceneManager.LoadScene("Menu");  // Asume que el men� se llama "MenuPrincipal"
    }

    // M�todo para salir del juego (puede ser llamado desde el bot�n del panel Game Over)
    public void SalirDelJuego()
    {
        Application.Quit();  // Cierra el juego
    }

    void Update()
    {
       if (Input.GetMouseButtonDown(0))  // Detectar clic del jugador
       {
          StartCoroutine(LoadLevel());
       }
    }

    IEnumerator LoadLevel()
    {
        if (puertaRoja.IsUnlocked() && puertaAzul.IsUnlocked())
        {
            transitionAnim.SetTrigger("End");
            yield return new WaitForSeconds(1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            transitionAnim.SetTrigger("Start");
        }

    }
}
