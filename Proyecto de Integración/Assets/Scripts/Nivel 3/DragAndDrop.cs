using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    private bool isDragging = false;  // Indica si el objeto est� siendo arrastrado
    private Vector3 offset;           // Offset entre el mouse y el objeto
    private Camera mainCamera;        // C�mara principal para capturar la posici�n del mouse
    private Rigidbody2D rb;           // Rigidbody de la llave
    private Vector3 startPosition;    // Posici�n inicial de la llave

    public GameObject[] lifeSprites;  // Sprites de las vidas
    private int livesRemaining;       // Vidas restantes del jugador

    private bool resetInProgress = false; // Flag para evitar que el mouse recapture la llave tras el reset

    void Start()
    {
        mainCamera = Camera.main;                  // Obtener la c�mara principal
        rb = GetComponent<Rigidbody2D>();         // Obtener el Rigidbody2D de la llave
        startPosition = transform.position;       // Guardar la posici�n inicial
        livesRemaining = lifeSprites.Length;      // Inicializar las vidas
    }

    void Update()
    {
        if (isDragging && !resetInProgress) // Solo arrastrar si no est� en proceso de reset
        {
            // Obtener la posici�n del mouse en el mundo
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 targetPosition = new Vector2(mousePosition.x + offset.x, mousePosition.y + offset.y);

            // Usar MovePosition para mover la llave
            rb.MovePosition(targetPosition);
        }
    }

    private void OnMouseDown()
    {
        if (!resetInProgress) // Evitar interacci�n si la llave est� en proceso de reinicio
        {
            Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
            offset = transform.position - mousePosition;
            isDragging = true;
        }
    }

    private void OnMouseUp()
    {
        // Finaliza el arrastre
        isDragging = false;
        rb.velocity = Vector2.zero;  // Detener el movimiento cuando se suelta el mouse
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Pared")) // Verifica si colisiona con una pared
        {
            Debug.Log("La llave toc� una pared.");
            LoseLife();     // Resta una vida
            ResetKey();     // Reinicia la posici�n de la llave
        }
    }

    private void LoseLife()
    {
        if (livesRemaining > 0)
        {
            livesRemaining--;
            lifeSprites[livesRemaining].SetActive(false); // Desactiva un sprite de vida

            if (livesRemaining <= 0)
            {
                Debug.Log("Game Over");
                // Aqu� puedes activar un panel de "Game Over"
            }
        }
    }

    private void ResetKey()
    {
        // Inicia el proceso de reset
        resetInProgress = true;
        isDragging = false;          // Detener el arrastre si estaba activo
        transform.position = startPosition; // Teletransporta a la posici�n inicial
        rb.velocity = Vector2.zero;  // Detener cualquier movimiento

        // Retrasar la habilitaci�n del arrastre para asegurarse de que el jugador no la controle inmediatamente
        Invoke(nameof(EnableInteraction), 0.1f);
    }

    private void EnableInteraction()
    {
        resetInProgress = false; // Permitir que el jugador vuelva a interactuar con la llave
    }
}
