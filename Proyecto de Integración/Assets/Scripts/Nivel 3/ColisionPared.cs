using UnityEngine;

public class ColisionPared : MonoBehaviour
{
    public NivelController nivelController;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            nivelController.PerderVida();
        }
    }
}
