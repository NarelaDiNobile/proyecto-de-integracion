using UnityEngine;
using TMPro;

public class ContadorTiempo : MonoBehaviour
{
    public float tiempoRestante = 120f; // Tiempo en segundos
    public TMP_Text textoTiempo;
    public GameObject panelGameOver; // Panel de "Game Over"

    void Update()
    {
        if (tiempoRestante > 0)
        {
            tiempoRestante -= Time.deltaTime;
            textoTiempo.text = "" + Mathf.Ceil(tiempoRestante).ToString();
        }
        else
        {
            ActivarGameOver();
        }
    }

    public void ActivarGameOver()
    {
        panelGameOver.SetActive(true);
        Time.timeScale = 0; // Pausar el juego
    }
}
