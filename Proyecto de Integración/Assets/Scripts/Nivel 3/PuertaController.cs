using UnityEngine;
using TMPro;

public class PuertaController : MonoBehaviour
{
    public TMP_Text textoPuerta; // Texto de "Puerta desbloqueada"
    private bool isUnlocked = false;

    void Start()
    {
        textoPuerta.gameObject.SetActive(false); // Ocultar el texto al inicio
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((gameObject.name == "Puerta Roja" && collision.name == "LLave Roja") ||
            (gameObject.name == "Puerta Azul" && collision.name == "LLave Azul"))
        {
            textoPuerta.gameObject.SetActive(true); // Mostrar el texto
            Destroy(collision.gameObject);          // Eliminar la llave
            isUnlocked = true;                      // Marcar como desbloqueada
        }
    }

    public bool IsUnlocked()
    {
        return isUnlocked;
    }
}
